# md2020

This repository provides the content for the [Metadata 20/20 website](http://www.metadata2020.org/). This README provides details about the location for each of the sections of the website, and includes information about how to make changes and how to push those changes to the website.

_Useful reference information:_

* [Markdown formatting examples](/content/markdown-examples)
* [Website theme documentation](/themes/hugo-universal-theme)
* Repository structure
  * [content](/content) - _A majority of the content shown on the site._
  * [data](/data) - _The content for the home page._
  * [static](/static) - _The images used on the site._

_Information about how to affect site structure (like menus and home page headers) is not included in this README file._

# Deploying the site

The site deployment is generally automated through Travis CI by creating pull requests between different branches (live, staging, dev, etc). These Pull Requests trigger a script to be run which builds the website:

Branch | Description | URL
------ | ----------- | ---
live | The public website. | http://www.metadata2020.org/ *
staging | The staging website - this is where content is put prior to publication to preview how it will look in production. | http://staging.metadata2020.org/
master | The master branch is generally used for configuring new structural changes on the website. It will be rare that current new content is added to the master branch. | http://dev.metadata2020.org/ _Requires a password_

_ * Note: At the moment, the live website is not available at https, nor is it configured to work at http://metadata2020.org _

## Deploying locally

If you have checked out the repository, it is possible to run it locally before uploading it. This can be helpful for previewing your work as you are doing it. 

Each one of the site environments has its own configuration file, which can come in handy to allow for configuration changes without affecting the live site. Hugo (the framework for the website) by default is configured with one configuration, called config.toml (usually). Since the Metadata 20/20 site has three configurations, you can choose which of the three you want to use when rendering the site to your computer. 

To deploy the site locally, navigate from the terminal app to the folder containing your repository and type the following

`hugo server -w --config [CONFIG FILE NAME]`

In place of '[CONFIG FILE NAME]' use the file name for the configuration that you would like to preview:

`dev` - the development configuration
`staging` - the staging configuration
`live` - the live configuration

# Site Structure

## The Sections

This site uses a theme called "Universal Theme for Hugo" that provides the site styling. As a result, the content can just be added and managed in [Markdown](https://en.wikipedia.org/wiki/Markdown).

### Site subsections

There are five subsections of the site that have content that changes more regularly. These sections are managed in the [content](/content) folder in Github. In order of change frequency:

* The blog
* Event listing
* Resources
* Communities section
* Project section

### Home Page

The Home Page content can be found in the [data](/data) folder. It consists of the following:

* Carousel
* Blurb with each of the featured areas
* A set of testimonials

_There are also some sections that are part of the site parameters (so are not included in this README). They include the **menu picks**, **about Metadata 20/20 text**, and some of the other features shown on the homepage._

### About Page

The text for the About Page of the site can be found in the [about.md file](about.md)

## Editing the site

### Adding a blog post

All blog posts are included in the [content/blog](/content/blog) folder. To add a new post, you will do the following (using the Content Management Process described elsewhere - make sure that your changes are in the MASTER branch in github!)

1. Create a new Markdown file to the [content/blog/](/content/blog) folder with the naming convention of
`yyyy-mm-dd-post-title-separated-by-hyphens.md`

2. Add a header to the file (see the [README](/content/blog/README.md) in the `content/blog/` folder for more details): 
``` markdown
+++
title = "Are you our new Community Engagement Lead?"
date = 2018-10-19
draft = false
tags = ["communities"]
categories = ["Jobs"]
banner = "/img/banners/banner_painting.jpg"
thumb = "/img/banners/thumb_painting.jpg"
author = "Clare Dean"
+++
```
3. Add the images for the blog post banner and thumbnail to the [/static/img/banners](/static/img/banners) folder.

4. Add the body of the blog post to the file and save it.

5. Commit your changes to Github using the website publishing process (documentation to come...)

## Publishing Changes

Before reading this section, you should familiarize yourself with the [Crossref Deployment How To](https://staging.crossref.org/_editors/deployment-how-to/) that describes the general structure of the repository branches and their purpose. For the purposes of this README, we will assume that you are making changes only to content (not the site structure.) As a result, you will be working mainly with the **Staging Branch** in the repository.

Usually a change that you are making affects several files. For example, if you are writing a new blog post, you will add the post, and will also need to add a banner and thumbnail image, and may even need to update other pages. To make this change clearer to others, it is helpful to package your changes using a "pull request." The instructions here describe this process. 

1. **Make a temporary branch for your content changes**.
Before getting started, you should always [create a new branch](https://help.github.com/en/articles/creating-and-deleting-branches-within-your-repository) from the **staging branch**. You do this so that you can group your changes together, making it easier to see the changes made, and provide notes about the change, for example, why the change was made.

2. **Make your changes in the branch**.
Once you are sure that you are making changes on the correct branch, edit, add and delete (if necessary) the files for your change. (See above for more information about changing & editing content.)

3. **Commit changes along the way**.
As you are making changes, commit your work into your local version, providing comments along the way. This approach will allow you to more easily see specific changes that you make.

4. **Push your changes online to Github**.
Along the way you should push your changes to Github online. The main reason for this suggestion is to backup the changes that you are making in case anything happens to your computer, but it is also helpful in making your changes visible to others in case you need help. A good practice is to push your changes at least daily if you are working on something that will take you more than a day.

5. **Package your changes as a pull request**.
Once you have made all of your changes, you will package them together and provide a descriptive comment about them. To do this, 
   *  Go to the repository on Github and switch to the temporary branch that you made and click the "New pull request" button to open a pull request. 
   * Ensure that you have selected the **staging branch** as your base - you likely will need to change default behavior by changing the base from **master** to **staging**
   * Write a descriptive title and some notes about your change. Consider if it was three years from now and you were looking for this change. What text would be helpful for you to know what this change is without having to look at the actual changes that you made?
   * Click the "Create pull request" button to start the process.

6. **Review build tests**
The system will automatically check the changes that you have made to ensure that they don't "break the site". There are two checks that are performed - one for your pull request (which will need to pass before you move on to the next step), and one for your temporary branch (which will likely fail, and you don't need to worry about it.) Once the check for your pull request has passed, move on to the next step.

7. **Merge your changes into staging**
Once your pull request has passed the build tests, you are ready to merge your changes so that you can see them on [http://staging.metadata2020.org/](http://staging.metadata2020.org/)
   * Click the "Merge pull request" button - you can add a comment here if desired.
   * Wait for the build to complete, and recheck the success of the build using Travis CI
   * View the changes on the [staging site](http://staging.metadata2020.org/). _NOTE: If you added a blog post that has a publish date that is in the future, you will need to wait until that date to see it published on the staging site. To see it now, you can temporarily  update the date to today or a day in the past. Don't forget to change it back before pushing your changes into the live branch, though!_

