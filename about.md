+++
title = "About"
draft = false
date = "2017-07-08"
banner = '/img/backgrounds/Orange_Banner.jpg'
style = "about"
+++

### What?

Metadata 20/20 is a collaboration that advocates richer, connected, and reusable, open metadata for all research outputs, which will advance scholarly pursuits for the benefit of society.

### How?

We aim to create awareness and resources for all who have a stake in creating and using scholarly metadata. We will demonstrate why richer metadata should be the scholarly community's top priority, how we can all evaluate ourselves and improve, and what can be achieved when we work harder, and work together.

### Why?

> Richer metadata fuels discovery and innovation.
> Connected metadata bridges the gaps between systems and communities.
> Reusable, open metadata eliminates duplication of effort.

When we settle for inadequate metadata, none of this is possible and everyone suffers as a consequence.

{{% imagebar "/img/fishes.jpg" %}}

### Who?

{{% row %}}

{{% column %}}
You! We will be offering many opportunities for getting involved: workshopping, listening, creating, evaluating, and developing. This page lists the founding team, advisors, and early advocates.

Please let us know how you can help.

{{% /column %}}

{{% column %}}
The members of the core team are:

* **Ginny Hendricks, Founder and Director**
* **John Chodacki, Chair of Advisory Group**
* **Clare Dean, Outreach and Communications**
* **Paula Reeves, Insights and Creative**
* **Ed Pentz, Sponsor**
{{% /column %}}

{{% /row %}}


{{% divwrap lowlight %}}
#### Advisors

{{% row %}}

{{% column %}}
* Cameron Neylon, Curtin University
* Caroline Sutton, Taylor & Francis
* Dario Taraborelli, Wikimedia Foundation
* Eva Mendez Rodriguez, UC3M / OSPP / DCMI
* Ed Pentz, Crossref
* Genevieve Early, Taylor & Francis
* Ginny Hendricks, Crossref
* John Chodacki, California Digital Library - Chair
* Juan Pablo Alperin, Public Knowledge Project
* Kristen Ratan, Coko Foundation
* Carly Strasser, Moore Foundation
{{% /column %}}

{{% column %}}
* Laure Haak / Alice Meadows, ORCID
* Mark Patterson, eLife
* Mike Taylor, Digital Science
* Natalia Manola, OpenAIRE
* Patricia Cruse, DataCite
* Paul Dlug, American Physical Society
* Roy Tennant, OCLC
* Scott Plutchak, University of Alabama
* Stefanie Haustein, University of Ottawa
* Steve Byford, JISC
{{% /column %}}

{{% /row %}}

{{% /divwrap %}}
{{% divwrap lowlight %}}

#### Advocates

These are some of the people who have helped so far, either by running workshops, or drafting resources.

{{% row %}}

{{% column %}}
* Judy Ruttenberg, SHARE
* Alice Meadows, ORCID
* Jennifer Kemp, Crossref
* Laura Wilkinson, ORCID
* Christina Hoppermann, Springer Nature
* Jabin White, JSTOR
* Gurdeep Mattu, Bloomsbury Publishing Plc
* Barbara Chen, Modern Language Association
* Howard Ratner, CHORUS
* Stephanie Lovegrove, Silverchair
* Ted Habermann, HDF Group
* Lai Kei Pang, University of Auckland
* Stewart Marshall, CAB International
* Stacy Masucci, Elsevier
{{% /column %}}

{{% column %}}
* Adrian-Tudor Panescu, Figshare
* Lettie Conrad, Consultant
* Juliane Schneider, Harvard Catalyst
* Charlie Rapple, Kudos
* Rachael Lammey, Crossref
* Nicky Agate, Modern Language Association
* Julie Zhu, IEEE
* Marianne Calihanna, Cenveo Publisher Services
* Wes Royer, Silverchair
* Michael Upshall, UNSILO
* Mark Leggott, Research Data Canada
* Christopher Erdmann, North Carolina State University
* Jonathan Glasspool, Bloomsbury Publishing Plc
* Christina Gifford, Elsevier
{{% /column %}}

{{% /row %}}
{{% /divwrap %}}

_Note that listing an individual contributor doesn't necessarily mean an official endorsement from their employer._

---
Please [email us](mailto:info@metadata2020.org) if you'd like to get involved.
