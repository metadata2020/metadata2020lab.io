+++
title = "Project Plans"
date = 2018-05-04
draft = false
tags = ["projects"]
categories = ["projects"]
banner = "/img/banners/banner_balls.jpg"
thumb = "/img/banners/balls_thumb.jpg"
author = "Clare Dean"
+++

### Summary

Through discussions and additional work, Metadata 2020 project participants have now created 4-6 month project plans for intended execution between May and October 2018, and in some cases have already started to advance the work. Participants are also benefiting from a useful exchange of ideas in meetings. Thanks to everyone giving up their time and contributing their expertise to this important work.


### 1. Researcher Communications
**Led by: Carly Strasser**

* Review existing surveys and articles surrounding researcher attitudes to metadata
* Consider assigning a student to conduct a literature review
* Examine publishers who have improved metadata over last 2 years, and interview them about possible reasons (including how they may have encouraged authors to deposit more complete metadata)
* Conduct informal interviews to researchers in different fields to inform survey questions
* Create survey
* Find channels for survey distribution
* Collect results

[More](/projects/researcher-communications/)


### 2. Metadata Recommendations and Element Mappings
**Led by: Jim Swainston**

* Identify list of metadata schemas that are in use (“Metadata Schema Index”)
* Share list of schemas with MD2020 community to see if there are any missing and to get feedback on which are most used
* Collaborate with Project 5 to discuss list of tags/elements in each of the schemas of interest and identify concepts that cut across them
* Map concepts and element names in a table
* Produce diagram/poster to highlight similarities and differences

[More](/projects/mappings/)


### 3. Defining the Terms We Use About Metadata
**Led by: Scott Plutchak**

* Pilot researcher interviews surrounding metadata at Experimental Biology conference
* In consultation with other project and community groups, refine questions per community into a fuller survey for circulation surrounding metadata terminology
* In alignment with other community groups, distribute survey(s) and analyze results
* Contribute to central list of schema (P.2)
* Contribute to P.5's list of elements where needed
* Considering developing a “glossary” to clarify terminology

[More](/projects/definitions/)


### 4. Incentives for Improving Metadata Quality
**Led by: Fiona Counsell**

* Discuss illustration of metadata flow through scholarly communications in consultation with community groups
* In consultation with community groups, start to develop fuller lists of incentives
* Create a detailed list of outputs for phase 2 of the project
* Gather material for re-use in business cases
* Align incentives list with P.5’s best practice guidelines
* Discuss outreach & education plans for time when first content is available for distribution, including proposals for upcoming conferences

[More](/projects/incentives/)


### 5. Shared Best Practice and Principles
**Led by: Howard Ratner and Jennifer Kemp**

* Share and discuss list of tags/elements
* Survey Metadata 2020 population for best practices resources
* Catalog and publish list of best practices resources
* Create set of principles (e.g., FAIR)
* Follow up with hosting platforms and consultants that work with them to include what is included in RFPs, etc.  
* Create white paper / statement to the community

[More](/projects/shared-practice/)


### 6. Metadata Evaluation and Guidance
**Led by: Ted Habermann**

* Populate Schemas, Tools and Guidance Information
* Review existing evaluation tools and identify what they are evaluating (completeness, consistency, linking, quality, other).
* Identify advantages/disadvantages of current evaluation tools and make recommendations for use
* Together with Project 5, create a guidance document for evaluation tool use for multiple communities and/or create a white paper or statement to the wider scholarly communications community
* Consider where there might be gaps in evaluation resources and where relevant, scope out the needs for ideal additional tools
* Complete Project 5’s best practices resources survey

[More](/projects/evaluation-guidance/)
