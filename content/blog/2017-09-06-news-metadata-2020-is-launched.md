+++
title = "News Release: A New Metadata Collaboration is Launched"
date = 2017-09-06
draft = false
tags = ["launch"]
categories = ["news release"]
banner = "/img/banners/banner-lanterns.jpg"
thumb = "/img/banners/banner-lanterns-thumb.jpg"
author = "Metadata 2020"
+++

### Metadata is the engine that drives the discovery, use, and the reuse of research information

A group of people from all over the world have joined forces to form Metadata 2020, with the goal of rallying and supporting the community around the critical issue of sharing richer metadata for research communications.

Metadata 2020 is a collaboration that advocates for richer, connected, and reusable metadata for all research outputs. Why?

* Richer metadata fuels discoverability and innovation.
* Connected metadata bridges the gaps between systems and communities.
* Reusable, open metadata eliminates duplication of effort.

When we settle for inadequate metadata, none of this is possible and everyone using those research outputs suffers as a consequence.

> Most people wouldn’t think: ‘Well if we can fix this metadata we can find a cure for a terrible illness.’ If we can find a way to connect those dots, that would be huge. Nobody is asking: ‘What is the cost to society?’

This collaboration will engage publishers, aggregators, service providers, librarians, funders, and researchers to participate by committing to improving the quality and interoperability of their metadata. The aim is to both raise awareness of the importance of scholarly metadata, and provide resources for all who have a stake in creating and using it. Metadata 2020 will demonstrate why richer metadata should be the scholarly community’s top priority, how everyone can self-evaluate and improve, and what can be achieved when the community works together toward a common goal. Metadata 2020 will offer workshops, resources, and evaluation tools.

Crucially, Metadata 2020 will facilitate communication between the different communities involved in scholarly communications, sharing stories and resources to provide education and support to everyone seeking to improve their metadata. Metadata 2020 representatives will be speaking at a variety of conferences and events this year, including the Charleston pre-conference and London Info International.

The current list of [advisors and founders](/about) is updated on our website and the [background story is on our blog](/blog/2017-09-06-introducing-metadata-2020/).

Follow [@Metadata2020](https://twitter.com/metadata2020) on Twitter, and [contact us](mailto:info@metadata2020.org) with any questions.
