+++
title = "New beginnings for 2019"
date = 2019-02-24
draft = false
tags = ["projects", "workshops", "metadata", "insights"]
categories = ["strategy"]
banner = "/img/banners/banner_orange-abstract.jpg"
thumb = "/img/banners/thumb_orange-abstract.jpg"
author = "Ginny Hendricks"
+++

2018 was quite a year: Metadata 2020 was on the agenda at [17 conferences, workshops and meetings](../../events/); community groups formed into [six cross-stakeholder projects](../2018-04-09-projects-update/); the projects got to work to better understand metadata challenges; and we held two end-of-year-one in-person workshops with 50 people in New York and London to share things out and discuss the 2019 agenda. Over 200 people participated in some way, whether through signing up for the mailing list, chatting on Slack, or attending online meetings and webinars. Our six project groups discussed and conducted research to learn more about how we all use metadata, and how we can make metadata more effective and efficient for these uses.  

## 2018 Project Progress

Some examples of what the projects have been exploring:

* General principles that guide how we are thinking about metadata
* General concepts/ definitions that many metadata scheme share.
* The metadata best practices that are being used, and a way to put them into context
* Metadata use cases 
* A survey to understand researcher and scholars experiences and attitudes toward metadata
* An interactive tool to demonstrate how these outputs influence and work with each other

For 2019, we are planning to share what we've learned, and invite input and feedback to broaden our understanding. Soon we will be announcing a webinar series that will kick off this process. If you are not yet involved in Metadata 2020 and would like an invitation to the webinar series once it gets started, please [let us know](mailto:info@metadata2020.org).

## Metadata 2020 Workshops

![Flow diagrams created](/img/figures/ny-flow_thumb.jpg)

Toward the end of 2018 we held two Metadata 2020 Workshops that gave us an opportunity to work face to face. All attendees agree that this dedicated time helped to advance the work of all of the projects, and highlighted new opportunities for cross pollination of ideas. These insights are key inputs to our approach for 2019.

### New York Workshop

_September, 2018._ The two-day New York Workshop was held at space generously donated by Springer Nature. White board markers and post-its in hand, the 20 participants explored metadata best practices and stories, communication approaches, flow diagrams, books and NRO use cases, and community engagement strategies. We even tried out a new Metadata game!

### London Workshop

_October, 2018._ Nearly 30 people made their way to City, University of London who hosted our London Workshop. Here we built on our discussions from New York, refining and expanding the metadata flow diagrams and exploring the personas that are affected by metadata and the benefits they each can expect.

## Team updates

The end of 2018 also brought some change to the Metadata 2020 team. Our project lead, Clare Dean, has moved on to a new position. We thank her for her many contributions and wish her well in her future endeavors. We welcome Laura Paglione who has joined the team to pick up where Clare has left off. She will be providing project management and coordination support, and you may even see her giving a presentation or two. Laura has already started working with the project teams to finalize their 2018 outputs and prepare them for sharing with the community.
