+++
title = "Call for Community Input: Metadata 2020 Draft Principles"
date = 2019-05-13
draft = false
tags = ["metadata", "insights", "projects", "standards"]
categories = ["recommendations"]
banner = "/img/banners/banner_puzzle.jpg"
thumb = "/img/banners/thumb_puzzle.jpg"
author = "Jennifer Kemp and Howard Ratner"
+++

Many of you are probably aware that Metadata 2020 has a [project group](../../projects/shared-practice/) working on Best Practices and Principles. After many months of collaboration, we are happy to share a draft of the Principles for community input. 

These aspirational Metadata 2020 Principles were designed to encompass the needs of our entire community while ensuring thoughtful, purposeful, and reusable metadata resources. They advocate for all of us to be good metadata citizens. They provide a foundation for considering related work from Metadata 2020 and must be interpreted within the legal and practical context in which the communities operate. A forthcoming set of Best Practices will illustrate use cases and provide some concrete action items to support the Practices.

Though these guiding Principles were drafted by stakeholders from across scholarly communications we feel strongly that seeking comments from the widest possible audience is the right approach for such a key, shared resource. The final version of these Principles will be central to the forthcoming culminating work of Metadata 2020 so your input will carry through beyond this particular resource. 

Comments, questions and suggestions on the Principles are welcome from any and all in scholarly communications. If you’re not familiar with metadata, all the better. These Principles are intended to guide the broadest possible cross-section of our community in improving research communications, publishing and discoverability. If they don’t speak your role in scholarly communications, we want to hear from you. 

<table>
  <thead>
    <tr><th colspan=2 style="line-height:1.8; font-size:1.5em;">METADATA PRINCIPLES</th></tr>
  </thead>
  <tbody>
    <tr><td colspan=2 style="font-size:1em;">&nbsp;<br/>For metadata to support the community, it should be</td></tr>
    <tr><td colspan=2 style="font-size:1em;"><b style="line-height:1.6; font-size:1.2em;">COMPATIBLE:</b> provide a guide to content for machines and people</td></tr>
    <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="font-size:1em;"><em>So, metadata must be as open, interoperable, parsable, machine actionable, human readable as possible.</em><br/>&nbsp;</td></tr>
    <tr><td colspan=2 style="font-size:1em;"><b style="line-height:1.6; font-size:1.2em;">COMPLETE:</b> reflect the content, components and relationships as published</td></tr>
    <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="font-size:1em;"><em>So, metadata must be as complete and comprehensive as possible.</em><br/>&nbsp;</td></tr>
    <tr><td colspan=2 style="font-size:1em;"><b style="line-height:1.6; font-size:1.2em;">CREDIBLE:</b> enable content discoverability and longevity</td></tr>
    <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="font-size:1em;"><em>So, metadata must be of clear provenance, trustworthy and accurate.</em><br/>&nbsp;</td></tr>  
    <tr><td colspan=2 style="font-size:1em;"><b style="line-height:1.6; font-size:1.2em;">CURATED:</b> reflect updates and new element</td></tr>
    <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="font-size:1em;"><em>So, metadata must be maintained over time.</em></td></tr>
  </tbody>
</table>

---

**Please share your thoughts with us by Wednesday June 5th by emailing us at [info@metadata2020.org](mailto:info@metadata2020.org) or commenting below.**

---

### About the authors

_Jennifer Kemp and Howard Ratner are co-chairs of Metadata 2020 [Project 5: Shared Best Practices and Principles](../../projects/shared-practice/). Metadata 2020 is a collaboration that advocates richer, connected, and reusable, open metadata for all research outputs, and which will advance scholarly pursuits for the benefit of society. We have created these Principles based on existing best practices to complement and support FAIR data principles with an interpretation inclusive of both data and metadata._
