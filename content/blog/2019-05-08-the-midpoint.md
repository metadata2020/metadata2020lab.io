+++
title = "The midpoint - reflecting back; looking forward"
date = 2019-05-07
draft = false
tags = ["communities", "insights", "projects", "recommendations", "scope"]
categories = ["strategy"]
banner = "/img/banners/banner_asphalt-dark-dawn-531321.png"
thumb = "/img/banners/thumb_asphalt-dark-dawn-531321.jpg"
author = "Laura Paglione"
+++

Metadata 2020 was established in 2017 with a bold mission to facilitate the collaboration of all involved in scholarly communications to consistently improve metadata to enhance discoverability, encourage new services, and create efficiencies, with the ultimate goal of accelerating scholarly research.  As our name implies, we also scheduled a 2020 deadline for our work.

![Metadata 2020 Roadmap](/img/figures/roadmap.png)

As we consider our progress at the project midpoint, we have a full journey to be proud of. Our 2017 workshops and listening tour about this metadata mission led to the establishment of [six community working groups](../../communities/) that helped provide insight about the metadata challenges as experienced from their vantage points. Each group articulated a set of challenges and opportunities, and created a vision statement of what they hope for the impact of metadata in the future.

By early 2018, we recognized that several of the challenges and opportunities found by the community groups were good candidates for project work. We established six projects with participants from different communities. Each project group was charged with developing a plan to better understand and/or address challenges found in metadata. Our workshops at the end of 2018 provided the opportunity for cross-project collaboration and sharing.

During 2019 our volunteer project teams, and community contributors will be sharing the outputs from last year’s work. Our work falls into two types of outputs:

* **Guidance**: Outputs that represent our thinking about metadata and how we feel that it should be characterized, managed, thought about, and valued.
* **Understanding**: Outputs that provide insights into how we have classified and made sense of what currently exists and the comparison of these works to each other.

## What to expect

Over the next several weeks we will be sharing several of these outputs:

* **Metadata Principles**: What does it mean to have “richer metadata?” How does context affect this equation?
* **Metadata Literature Review**: How are academics thinking about metadata and characterizing its effectiveness? Where are the gaps in our insight that could benefit from further study?
* **Metadata Insights & Attitudes Survey**: What do librarians, publishers, repositories and researchers and others think about metadata? What might help them find more value?

## Other Metadata 2020 Outputs

### Guidance

Outputs that represent our thinking about metadata and how we feel that it should be characterized, managed, thought about, and valued.

_None published yet_

### Understanding

Outputs that provide insights into how we have classified and made sense of what currently exists and the comparison of these works to each other.

#### [Metadata Best Practices](../../resources/metadata-best-practices/)

_Initially published June 2018_

This output consists of links to existing metadata guidelines and best practices in order to shed light on what currently exists and whether they are applicable to the scholarly communications lifecycle. Best practices, in general, reflect agreed upon standards that help an industry or field to do its job better. The scholarly communications lifecycle works better when best practices are known and used by the community at all points of the cycle. The list is not meant to be comprehensive. If you think a resource is missing from this list, please let us know! Email us at info@metadata2020.org
