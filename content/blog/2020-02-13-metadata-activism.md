+++
title = "Metadata Activism"
date = 2020-02-13
draft = false
tags = ["metadata", "big-ideas", "conferences"]
categories = ["research", "projects", "activism"]
banner = "/img/banners/banner_earth.png"
thumb = "/img/banners/thumb_earth.png"
author = "Laura Paglione and John Chodacki"
+++

![Without metadata... Zero Hunger will take longer](/img/figures/zero-hunger.png)

Metadata 2020’s activism work is based on the Big Ideas that can be enabled and enhanced by richer metadata - ideas like solving poverty, and eliminating hunger.

We have developed a set of [Metadata Principles](/resources/metadata-principles/) that are helpful in quantifying and evaluating contributions that metadata is making toward these Big Ideas. These Principles quantify what it means to have richer metadata, metadata that is **Compatible** (provide a guide to content for machines and people), **Complete** (reflect the content, components, and relationships as published), **Credible** (enable content discoverability and longevity), and **Curated** (reflect updates and new elements). Based on these Principles, we also have developed a set of Metadata Practices that outline the actions needed by individuals and organizations to achieve these Principles to affect the Big Ideas. 

During the last week of January, we kicked off our latest round of metadata activism at the [PIDapalooza conference](https://www.pidapalooza.org/upcoming-festival) in Lisbon, Portugal. With two opportunities to get participants engaged we practiced some participatory community and action building.

## Big Ideas with the Tree of PID-ful Life

_[10.5281/zenodo.3661233](https://doi.org/10.5281/zenodo.3661233)_

What is a Big Idea that can be enabled by richer metadata? This is the question that this interactive exhibit sought to answer. Participants were invited to add their own big ideas to this tree that represents the growth of rich metadata to support these ideas. In addition to being really beautiful to look at, the tree by the end of the conference was filled with inspiring visions of a world rich with PIDs and metadata.

![Photo of the Tree of PIDful Life](/img/figures/TreeOfPIDfulLife.jpg)

## Be a Metadata Activist

_[10.5281/zenodo.3661228](https://doi.org/10.5281/zenodo.3661228)_

During a 30-min session in the big conference room, participants identified the Big Ideas that they wanted to affect, and created action pledges of specific metadata practices that they will do in the near term.

To help formulate action pledges, we discussed [Metadata Personas](/resources/metadata-personas/) and how practices are shaped by the personas we assume. When the Metadata 2020 community groups developed the Personas, they considered the flow of metadata between individuals and systems, and recognized that people and systems take on one or more personas based on what they are doing at any one moment. In addition, the actions performed by each Persona are quite similar regardless of industry, title or background. Using this role-based approach is a useful construct for thinking about the actions we individually can take to improve metadata.

* **Metadata Creators** provide descriptive information (metadata) about research and scholarly outputs.
* **Metadata Curators** classify, normalize, and standardize this descriptive information to increase its value as a resource.
* **Metadata Custodians** store and maintain this descriptive information and make it available for consumers.
* **Metadata Consumers** knowingly or unknowingly use the descriptive information to find, discover, connect, and cite research and scholarly outputs.

The actions of the pledges were based on the list of Metadata Practices that we are developing that fall into 8 broad groups:

1. Connect to what already exists
2. Adopt schema best practices
3. Honor the strategic nature of metadata
4. Prepare for evolving needs
5. Facilitate discoverability
6. Adopt an attitude of continuous improvement
7. Act on issues when you find them
8. Take a personal role in making metadata richer

Their pledges were inspiring. What Big Idea do you feel needs more attention and richer metadata, and what would your pledge be to affect change?
