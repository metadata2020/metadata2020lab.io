+++
title = "Updates on Metadata 2020 Projects"
date = 2018-04-09
draft = false
tags = ["projects"]
categories = ["projects"]
banner = "/img/banners/banner_stairs.jpg"
thumb = "/img/banners/thumb_stairs.jpg"
author = "Clare Dean"
+++

### Summary

The first meetings for the Metadata 2020 [projects](/projects/) have now taken place. They focused on the breadth and scope of each project, discussing the crossovers with other projects, and potential overlaps and need for synchronization with other metadata-related initiatives in scholarly communications.


### 1. Researcher Communications
**Chaired by: Carly Strasser**

This group discussed the significance of communicating with researchers to all scholarly communications community groups - Publishers, Librarians, Data Publishers and Repositories, Service Providers, and Funders. They reaffirmed the importance of developing consistent messaging for researchers, with an acknowledgement that the content of their communications would be informed by the outcomes of all other projects, but most significantly, ‘Defining the Terms we Use About Metadata’, ‘Shared Best Practice and Principles’, and ‘Metadata Evaluation and Guidance’. There were also discussions surrounding the need to incentivize researchers, including a diverse array of carefully selected case studies for researchers in different disciplines.

More detailed discussions and plans for future meetings will be categorized as follows:

* What are researchers asking to know?
* What do researchers need to know that they don’t know to ask?
* How do they want to receive the information?
* Who do they want to receive the information from?
* How will we know the information is valuable?

Action items for the next meeting include:

* Consider crossover projects
* Consider possible deliverables or a structure for project going forward
* Contribute to/adjust meeting notes

[More](/projects/researcher-communications/)


### 2. Metadata Recommendations and Element Mappings
**Chaired by: Ted Habermann**

This group’s early discussion centered around the different ways participants have had to practically map between standards, and ways they may have used new semantic web technologies to map between concepts.  The group agreed that they need to collect specific examples of using tools and methods to connect metadata or representations of dialects and/or recommendations; in addition to a collection of recommendations and guidelines on the different mechanisms used for mapping.

The group needs to gather more information and examples about mapping methods and their adoption across scholarly communications in order to gain a deeper understanding about their effectiveness. Action items for the next meeting include:

* Consider crossover projects
* Contribute to/adjust meeting notes

[More](/projects/mappings/)


### 3. Defining the Terms We Use About Metadata
**Chaired by: Scott Plutchak**

With an understanding that there are many different terms for metadata types and uses of terminology, this group discussed the need to address the main challenge of establishing  consistency, and ways to map terms across disciplines.  Acknowledging that there are other initiatives undertaking similar activities for more particular uses in scholarly communications, the group decided that one of their main tasks would be to coordinate efforts between these initiatives to aim for consistency between them. It was recognized that there is the need to map different definitions of the same terms, and therefore a close integration with Project 5’s list of elements, and also likely Project 2’s element mappings.

This group have decided that it is important to start talking with researchers in a variety of fields about the different ways they define metadata, and have decided to pilot an information-gathering approach at the upcoming Experimental Biology conference, where two project participants will be present. Action items for next meeting include:

* Consider elements for toolkit to take to Experimental Biology conference
* Gather examples of similar types of projects
* Review draft of elements list from Project 5

[More](/projects/definitions/)


### 4. Incentives for Improving Metadata Quality
**Chaired by: Ginny Hendricks**

This group started the project by re-establishing the purpose of the group to create stories and use cases about the importance of metadata to empower individuals at their organizations with information to help influence decision makers. There was a discussion about the types of problems they will identify and what kinds of organizations these will effect; and that we need big-picture ideas to inspire a variety of people in scholarly communications, alongside more specific detail-oriented stories to inspire specific action. This project has been identified as the ‘vision’ piece of Metadata 2020, using the information from all of the other projects to communicate to the wider scholarly communications community. This group left with an understanding of future areas for consideration:

* What is the definition of good metadata?
* What opportunities does it afford?
* How do you account for the cost of having to deal with bad metadata (developer hours, project delays)?

[More](/projects/incentives/)


### 5. Shared Best Practice and Principles
**Chaired by: John Chodacki**

This group faces the challenge of establishing best practice and principles surrounding metadata capture, use, reuse and distribution without focusing on individual schema or standards, but with enough specificity for relevance and to ensure maximum engagement. In their first meeting, they decided that a sub-group would establish a list of elements, for each project group member to assign their practices to. Discussions around these individual elements will eventually lead to the formation of a best practice document in addition to overarching statements about best practice.  The central list of elements will also be used by other groups, including Projects 2, 3, and 6.  The group have also decided that case studies will also be important to communicate best practice, and therefore will be interwoven with Projects 1 and 4. The primary action item for this group is:

* Sub-group meeting to create a list of metadata elements.
    
[More](/projects/shared-practice/)


### 6. Metadata Evaluation and Guidance
**Chaired by: Ted Habermann**

This meeting focused on defining the parameters of the project, with discussions around existing evaluation tools or workflows, and the accessibility of those tools to the group and others. The group discussed the difference and challenges between the evaluation of metadata completeness and metadata quality; and the difference between metadata for discovery and metadata for access.

There was an acknowledgement that the group would need to discuss evaluation methods regardless of containers. Additionally, there was some discussion about the massive disparity in publisher metadata quality and consistency.  

[More](/projects/evaluation-guidance/)

---

#### About the author

Clare Dean is an independent Marketing and Communications Consultant, and the Outreach Manager for Metadata 2020. She has worked for Ashgate, John Wiley & Sons, Emerald Group Publishing, and most recently as the Director of Communications for Elementa: Science of the Anthropocene. She now serves a variety of types organizations in scholarly communications; including publishers, service providers, research institutions, and professional societies. She can be reached at [cdean@metadata2020.org](mailto:info@metadata2020.org).
