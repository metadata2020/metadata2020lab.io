+++
title = "How do you Metadata?"
date = 2019-06-03
draft = false
tags = ["metadata", "insights", "projects", "standards"]
categories = ["research"]
banner = "/img/banners/banner_abstract-art-bright-2110951.jpg"
thumb = "/img/banners/thumb_abstract-art-bright-2110951.jpg"
author = "Alice Meadows and Michelle Urberg"
+++

Today we continue our communication of Metadata 2020 outputs as outlined in an [earlier post](../2019-05-08-the-midpoint/). Since 2018, our work has been primarily divided into six project groups; as co-chairs of the [Researcher Communications project](../../projects/researcher-commmunications/), we are happy to share an update on our work.

This project group is been charged with increasing our understanding of the attitudes and values that individuals have about metadata in scholarly outputs in order to help inform how we talk about metadata to this audience. So today we are launching a survey to gather feedback about metadata knowledge, attitudes, and usage in scholarly communications.  It is designed to gather input from all those who create, care for, curate and consume metadata. We want to hear from librarians, publishers and repositories, as well as researchers themselves. 

We enthusiastically invite you to participate in this voluntary survey, which will take about 15 minutes to complete. It is open to anyone  18 years old or over (no compensation for participants), and will be open though **June 21**! We will summarize our findings and share them with the community later this year.

<center><a href="https://www.surveygizmo.com/s3/4729698/Metadata2020-survey-blog"><button type="button" style="background-color: #93b7b3; border: none; color: #17222c; padding: 15px 25px; text-align: center;"><strong>TAKE THE METADATA 2020 SURVEY</strong></button></a></center>

#### HELP SPREAD THE WORD!
Please share this invitation to participate with others in your network or users of your products or services. 

---

### About the authors

_Alice Meadows and Michelle Urberg are co-chairs of Metadata 2020 [Project 1: Researcher Communications](../../projects/researcher-commmunications/). Metadata 2020 is a collaboration that advocates richer, connected, and reusable, open metadata for all research outputs, and which will advance scholarly pursuits for the benefit of society._
