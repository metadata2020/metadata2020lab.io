+++
title = "Metadata 2020’s Latest Publications"
date = 2020-08-05
draft = false
tags = ["insights", "projects", "resources"]
categories = ["resources"]
banner = "/img/banners/banner_stones.jpg"
thumb = "/img/banners/thumb_stones.jpg"
author = "Laura Paglione"
+++

Over the past several months we have been busy formally publishing  work that we did in 2019. As we enter the second half of 2020, the year our work is scheduled to wrap up, we have been thinking about how to ensure longevity for the insights, writings, and other contributions from the volunteers that have been working on Metadata 2020. 

We knew that we wanted to choose a location that would have longevity beyond what would be found on a completed project’s website and would continue to be freely accessible. Another key consideration is ensuring that these resources have appropriate metadata including persistent identifiers. Finally, we wanted the chosen location to be somewhere that could host the broad variety of outputs created, and encourage continued dialog about each. We selected Research Ideas and Outcomes (RIO) Journal for this task, and have started the process of publishing the Metadata 2020 work in a [special collection](https://riojournal.com/browse_user_collection_documents?collection_id=167).

---

*in our collection:*

**[A literature review of scholarly communications metadata](https://doi.org/10.3897/rio.5.e38698)**<br />Our first publication was highlighted in an [earlier blog post](/blog/2019-08-05-metadata-literature-review/). This peer-reviewed review of metadata literature highlights the challenges, opportunities and gaps experienced by key stakeholders including publishers, service providers, researchers, funders, librarians, and data curators/repositories.

**[Metadata 2020 Workathon Proceedings - Sept 2019](https://doi.org/10.3897/rio.6.e52878)**<br />We published these proceedings from our most recent workshop. This workshop informed our thoughts about creating an advocacy campaign to engage the community in establishing richer metadata. The participants discussed what a culminating project for Metadata 2020 might look like both to honor the work that has been done, and guide the action to continue once the Metadata 2020 project has completed.

**[Methods & Proposal for Metadata Guiding Principles for Scholarly Communications](https://doi.org/10.3897/rio.6.e53916)**<br />This methods paper describes the process and analysis behind the Metadata 2020 guiding principles. Taking a goal-oriented approach of what richer metadata provides, the principles of Compatible, Complete, Credible and Curated help to ensure that metadata supports the community.

**[Metadata 2020 Evaluation Projects](https://doi.org/10.3897/rio.6.e54176)**<br />Being able to evaluate the quality of metadata is a precursor to improving it. This paper provides a framework for connecting metadata dialects and community recommendations to evaluate metadata and understand what metadata guidance can be applied to greatest effect.

---

While these writings have a sense of finality to them through their publication, we are excited by RIO’s ability to highlight and annotate publications. We are hoping that you will continue the conversations and provide your own observations in these publications within the annotations that you provide.

And, these are just the start. There are several other publications in production that formalize the discussions that we’ve had and insights we have developed. 

---

*photo credit: [Sharon McCutcheon](https://www.pexels.com/@mccutcheon?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels) from [Pexels](https://www.pexels.com/photo/woman-holding-six-polished-stones-1147946/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)*