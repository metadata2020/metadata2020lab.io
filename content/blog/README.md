# About creating Blog Posts

## Naming Convention

Each post file needs to use the naming convention of

`yyyy-mm-dd-post-title-separated-by-hyphens.md`

## Header

Each post will need a header in the Markdown file which will serve as its template metadata. The header should look like the following:

_(from the [Are you our new community engagement lead?](/blog/2018-10-19-community-lead-job/) blog post.)_

``` markdown
+++
title = "Are you our new Community Engagement Lead?"
date = 2018-10-19
draft = false
tags = ["communities"]
categories = ["jobs"]
banner = "/img/banners/banner_painting.jpg"
thumb = "/img/banners/thumb_painting.jpg"
author = "Clare Dean"
+++
```

## Tag list

Blog posts can be marked with one or more tags. Tags are displayed on the site, and users may filter blog posts by tag. The tag is defined in the markdown header. Each tag is enclosed in quotes; multiple tags are separated by a comma.

``` markdown
tags = ["communities", "projects"]
```

The currently defined tags are:

* communities
* dialects
* insights
* launch
* metadata
* projects
* publishing
* recommendations
* researchers
* scope
* standards
* stories
* workshops

## Category list

Blog posts can be marked with one or more categories. Categories are displayed on the site and users may filter blog posts by category. The categories for a blog post are formatted similarly to the use of tags.

```markdown
categories = ["jobs", "strategy"]
```

The currently defined categories are:

* conferences
* data
* jobs
* news-release
* projects
* recommendations
* research
* stories
* strategy
* workshops

## Images in blog posts

Each blog post has a banner image and a thumbnail image. These images should be stored in the [/img/banners/](../../static/img/banners) folder.

**Sizes**: While the sizes don't matter that much (the theme will crop images appropriately), you will have more predictable results if you use images that are a consistent size and shape. Most images stored for blog posts have been cropped to the following sizes:

* Banner: 2500 x 600 px; resolution 72 px/in
* Thumbnail: 600 x 600 px

Images are sourced from [Pexels](https://www.pexels.com/) or other free stock photo site. When selecting images, please review the copyright both to ensure that it is available for use on a non-commercial site, and to review if attribution is necessary. If it is, please add the attribution directly to the image before upload using an image editor.

## Relative links

From a blog post, you can create a link to other parts of the site using relative links. Here are some common links that you may use:

* Go back to the home page: `../../`
* Go back to the blog listing: `../`
* Go to another blog post on the site: `../yyyy-mm-dd-name-of-the-post/` _(do not include the `.md` at the end of the name.)_
* Go to the events page: `../../events/`
* Go to the projects page: `../../projects/`
* Go to the communities page: `../../communities/`
