+++
title = "Action and Overlaps"
date = 2018-06-13
draft = false
tags = ["projects"]
categories = ["projects"]
banner = "/img/banners/banner_drums.jpg"
thumb = "/img/banners/thumb_drums.jpg"
author = "Clare Dean"
+++

### Action and Overlaps

Each project has now had at least three meetings, and activity for each is ramping up.  As it does so, several short term subgroups have emerged, and cross-project collaborations have formed.

### Survey

[P.1 ‘Researcher Communications’](http://www.metadata2020.org/projects/researcher-communications/) and [P.3 ‘Defining the Terms We Use About Metadata’](http://www.metadata2020.org/projects/definitions/) have both identified a need for a survey for researchers about their uses and needs of metadata. They will be forming a subgroup including representatives from each project to write the survey before consulting with the ‘Researchers’ Community Group to check for accuracy and relevance.

### Core Elements

[P.5 ‘Shared Best Practice and Principles’](http://www.metadata2020.org/projects/shared-practice/) recognized a need for a list of ‘core elements’ early during their first meeting.  They wanted to develop a list of bibliographic and non-bibliographic elements that they could discuss with the group and assign best practices of use to each.  As this list is close to finalization - and a subgroup will work through the list to confirm its accuracy - other projects are making plans to use the list in their project work. [P.2 ‘Metadata Recommendations and Element Mappings’](http://www.metadata2020.org/projects/mappings/) will be using the list of core elements to help them map between schemas.  [P.3 ‘Defining the Terms We Use About Metadata’](http://www.metadata2020.org/projects/definitions/) will be using the list of core elements to start building the Metadata 2020 definitions list and glossary of terms.

### Schemas Index

[P.6 ‘Metadata Evaluation and Guidance’](http://www.metadata2020.org/projects/evaluation-guidance/) and [P.2 ‘Metadata Recommendations and Element Mappings’](http://www.metadata2020.org/projects/mappings/) have developed an index of schemas used in scholarly communications. After identifying the most popular, P.2 will map between these, using the list of core elements to pin them together.

### Common List of Use Cases

As [P.5 ‘Shared Best Practice and Principles’](http://www.metadata2020.org/projects/shared-practice/) looks to underpin best practices with use cases, and [P.4 ‘Incentives for Improving Metadata’](http://www.metadata2020.org/projects/incentives/) explores ideas for demonstrating the importance of metadata improvements for organizations, a common need for a list of short use cases briefly describing uses or misuses of metadata across scholarly communications has been identified. This will also help P.4 in a project that they would like to work on with the Community Groups, and also perhaps through workshops to map and diagram the metadata flow through organizations.  A central list has been formed for all Metadata 2020 participants to contribute to (also open to those outside of Metadata 2020 who wish to help with this activity).


As always if you are not currently involved with Metadata 2020 but think you may be able to help us with one or more of these activities, please get in touch by [emailing me](mailto:cdean@metadata2020.org).
