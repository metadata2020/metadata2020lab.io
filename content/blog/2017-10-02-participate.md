+++
title = "Participating in Metadata 2020"
date = 2017-10-02
draft = false
tags = ["scope", "communities"]
categories = ["strategy"]
banner = "/img/banners/banner-blue-abstract-balls-spheres.jpg"
thumb = "/img/banners/banner-blue-abstract-balls-spheres-thumb.jpg"
author = "Clare Dean"
+++

Following the launch of Metadata 2020 at the beginning of September, we have been delighted to receive many enquiries from individuals across scholarly communications who are eager to participate in the collaboration. The [support is also clear through Twitter conersations](https://twitter.com/search?vertical=default&q=%40metadata2020&lang=en). Thank you to all of you who have offered your help!

As a result of the interest received, we have been able to start to form a variety of Community Groups for Publishers, Librarians, Researchers, Funders, Service Providers/Platforms and Tools, and Data Publishers/Repositories. These groups are tasked with defining the metadata problem from their specific community's perspective; discussing challenges and opportunities within their community; methods by which they can gather and aggregate detailed insights surrounding metadata; and ways to engage their communities further. They will largely define their own wider remits depending on the needs of their communities. Group representatives will also contribute their perspectives to the wider Metadata 2020 group so that we can begin to gain greater insight into the opportunities for groups to work together to resolve specific sticky issues.

## Do we need more Community Group members?

Yes, to some degree! Although we've active groups and many keen participants, we could use more representatives from funders to contribute their perspectives, as well as a few researchers. Although we have a good number of Publishers, we are lacking in perspectives from smaller publishers. If you are interested in getting involved in these groups, please do contact us. It is likely we will issue further calls for Community Group members, when we are working on specific projects and have gaps in expertise, so please keep an eye out on this blog, and sign up for newsletters.

Metadata 2020 is conducting the following workshops and speaking at events:

- **Frankfurt Book Fair** – Frankfurt, Germany; October 13th, 2.30pm
'Hot Spot' Presentation with Metadata 2020 Founder, Ginny Hendricks; Wednesday at N99 in Hall 4.2

- **Force 17** –  Berlin, Germany;  October 25 - 27
Hosted by Ginny Hendricks, John Chodacki, and Cameron Neylon; Friday 27th, 12-1:30pm

- **International Conference on Dublin Core and Metadata Applications** – Washington, D.C.; October 28, 8:30am - 12:30pm
[Metadata 2020: Accelerating Scholarly Discovery](http://dcevents.dublincore.org/IntConf/dc-2017)
Hosted by Juliane Schneider, Harvard Catalyst; Chuck Koscher, Crossref; and Patricia Feeney, Crossref

- **Charleston Preconference** – Charleston, SC; November 7, 9:00am - 12:00pm
[Sharing and Discovery 'Without Good Metadata, What is the Cost to Society? What Discoveries Are We Missing?'](https://www.charlestonlibraryconference.com/preconferences/)
Hosted by Jennifer Kemp, Crossref; Maryann Martone, Hypothesis; Lettie Conrad, Consultant; and Michelle Brewer, Wolters Kluwer

Please consider attending these meetings, learning more about Metadata 2020, and contributing your insights.

Finally, we are in the initial stages of gathering insights and stories surrounding Metadata. Please email me your thoughts at cdean@metadata20202.org.


### About the author

Clare Dean is an independent Marketing and Communications Consultant, and the Outreach Manager for Metadata 2020. She has worked for Ashgate, John Wiley & Sons, Emerald Group Publishing, and most recently as the Director of Communications for Elementa: Science of the Anthropocene. She now serves a variety of types organizations in scholarly communications; including publishers, service providers, research institutions, and professional societies.
