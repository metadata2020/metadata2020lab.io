+++
title = "Helping Researchers Understand Metadata"
date = 2020-01-08
draft = false
tags = ["metadata", "insights", "projects"]
categories = ["research", "projects"]
banner = "/img/banners/banner_deskchat.jpg"
thumb = "/img/banners/thumb_deskchat.jpg"
author = "Alice Meadows and Michelle Urberg"
+++

# Helping Researchers Understand Metadata 
### _The M2020 Researcher Communications Project_

The Metadata 2020 [Researcher Communications project](/projects/researcher-communications/) was set up with the goal of “Exploring ways to align efforts between communities that aim to increase the impact and consistency of communication with researchers about metadata.” More to the point, we have sought to answer this question: How can Metadata 2020 help researchers make better links between their published work and scholarly communications metadata?

We have re-examined our goal closely and see both successes and opportunities for future work.

### Researchers

This was a challenging project. Researchers are a diverse group, encompassing nearly all areas of study and nearly all topics of scholarly exploration. They are by no means homogeneous — different disciplines and communities use different terminology and have different metadata needs for scholarly outputs. For the most part, they are not motivated by their understanding of the benefits of high-quality metadata for their research, their organizations, and the wider scholarly communities that benefit from their research.

### Communities

Publishers, libraries, funders, and other organizations that host scholarly research outputs (e.g. data publishers and repositories) do recognize the value of robust metadata. Nevertheless, they do not want to alienate researchers by asking for too much metadata, especially if researchers have already provided it elsewhere. Likewise, different communities provide different types of messaging about metadata, leaving researchers confused about when metadata can be considered “complete” and what metadata should be accurate for publications and data sets.


### Exploring ways to align efforts between communities

Helping such a broad group to understand the value of metadata — and why it’s worth their effort to ensure that it is accurate and complete — takes a wide variety of approaches. Each community assisting researchers can help increase the impact of the metadata associated with their scholarly outputs. The work of this project explored what gaps currently exist in communications with researchers about metadata.

## Increasing the impact and consistency of communication

The two main outputs of this project group were directed toward understanding the gaps that exist in communication with researchers about metadata for research outputs, in order to better address these in future. The first is a literature review, entitled [A Literature Review of Scholarly Communications Metadata](https://doi.org/10.3897/rio.5.e38698), published in our [Metadata 2020 RIO Journal Collection](https://riojournal.com/collection/167/) in August 2019. It provides an overview of the metadata landscape and sets the stage for understanding gaps in our current knowledge about scholarly communications metadata. This work was led by Will James Gregg, Christopher Erdmann, Laura Paglione, Juliane Schneider, and Clare Dean. Sources consulted for the literature review can be found in a [ScienceOpen Literature Collection](https://www.scienceopen.com/search#collection/24c239e8-87a6-429c-a2ef-9036f1d49715) of resources about scholarly communications metadata. Stephanie Dawson, along with contributions from Will Gregg and Chris Erdmann, created the metadata literature collection in ScienceOpen; at the time of writing (January 2020) it has already been viewed nearly 3,000 times.

The second output is a survey and analysis of attitudes towards and understanding of metadata, which provides insights on perceptions of metadata value. The survey questions were developed through a collaboration of the full project team, and the survey was encoded and analyzed by Kathryn Kaiser, Maria Johnsson, Rachael Lammey, Alice Meadows, Laura Paglione, and Michelle Urberg.  The original plan was to survey researchers only, but it quickly became apparent that Metadata 2020 would benefit from feedback from a wider group of experts. Therefore, the scope was expanded to identify perceived gaps in researchers’ knowledge of metadata among the library, publishing, and repository management communities. The survey was open from June 5 to July 15, 2019, and promoted via email, social media, blogs, listservs, and online newsletters. The survey returned a total of 218 responses from 23 countries: 49 researchers, 87 librarians, 47 repository managers, and 27 publishers. The survey results will be published in RIO Journal when they are ready.

## Final assessment

We know a lot more about the gaps in researchers’ knowledge of metadata than when we started work on this project in 2017. The literature review and survey are key outputs that provide great insights; though ultimately, these outputs are just a starting place — data points — for creating more consistent communication about scholarly communications metadata. The next step is to use this data to craft better communication plans across the communities that support researchers in producing scholarly outputs.

We were lucky to work with a great group of volunteers on the [Researcher Communications project](/projects/researcher-communications/), and we are grateful for their support. Our thanks to everyone, especially to all those who worked on the literature review and survey — and, of course, to Laura Paglione, Metadata 2020’s chief coordinator, for helping keep us on track!  We have made impactful progress toward our goal.