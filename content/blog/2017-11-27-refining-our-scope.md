+++
title = "Refining our scope"
date = 2017-11-27
draft = false
tags = ["scope", "communities"]
categories = ["strategy"]
banner = "/img/banners/blog-squiggles-banner.jpg"
thumb = "/img/banners/blog-squiggles-thumb.jpg"
author = "Clare Dean"
+++

Embracing the enormity of metadata challenges in Scholarly Communications through Community Group discussions.

Metadata 2020 has always had big goals. From the earliest conversations between Metadata 2020 Director Ginny Hendricks and the Advisory Group she quickly convened, it was clear that there were a number of different directions a collaborative effort could take to improve metadata scholarly communications, the possibilities were seemingly endless. How, then, to narrow the scope and organize efforts?

Early Metadata 2020 workshops during the spring were received positively, although our sense that everyone had a different understanding about metadata and its importance was underscored, further indicating the enormity of our challenge. Upon launch in September 2017, bolstered by Alice Meadows’ Scholarly Kitchen post, we were delighted to receive a massive response from across the scholarly communications community with offers of participation.

In a matter of weeks we were able to form community groups, and convene our first group meetings. The community groups formed include: Publishers, led by Terri Teleen and Duncan Campbell of Wiley; Librarians, led by Juliane Schneider of Harvard Catalyst; Researchers, led by Cameron Neylon of Curtin University; Service Providers, Platforms and Tools, led by Marianne Calilhanna of Cenveo Publisher Services; Data Publishers and Repositories, led by John Chodacki of California Digital Library; and Funders.

The first two meetings for each group have now taken place, and in brainstorming, several interesting themes that we would like to take forward have emerged. In every group discussion, we grappled with the multiple directions an effort to improve metadata in scholarly communications could take, and discussed how to segment problems and focus efforts. Here is a brief summary of the approaches and discussions from each group thus far:

### Publishers

Discussions with publishers have involved a brainstorming of major issues with metadata collection, use and distribution at different levels throughout the scholarly communications chain. They have focused on discussing ways in which they might map these uses as a communication tool to those they collect metadata from, and integrate metadata with. This group has also shown an interest in discussing the balance of full metadata entry requirements with being respectful of author time; and ways in which they might communicate the importance of metadata entry for the use, re-use and discovery of research.

### Librarians

The Librarians group have found it helpful to break down metadata challenges and opportunities into three main categories: communicating with researchers about metadata; challenges we face in making use of metadata created by others, and librarian roles in applying metadata. The group has built a list of use cases and stories that fit under these broad categories (and continue to populate this as they receive information from workshops). A Metadata 2020 workshop at DCMI on Oct. 28, focused on the current metadata landscape, and the metadata-related tasks and activities that libraries perform. An outcome of that workshop was a list of these tasks/activities for reference in creating best practice resources. As a result of all of these discussions, the Librarian group have identified a few specific opportunities for development that they would like to explore, in addition to contributing to cross-community discussions.

### Researchers

Preliminary discussions from our researcher group focused on the need to reduce duplication of metadata entry; and how to engage more researchers in these discussions. A serious issue with all of these discussions is how to involve active researchers in conversations around systems and infrastructure that in an ideal world they would not have to notice! After two meetings it was proposed that we use the researcher group as an advisory board as other groups evolve new ideas involving researchers; and as a route to communicate more broadly with the community -  conducting surveys, interviews, and focus groups where needed. Unlike many of the other groups we can’t expect representative engagement, but we can work to reach out.

### Service Providers, Platforms, and Tools

The Service Providers group is trying to identify and organize how to define the metadata challenge from their perspective. The two meetings have been open discussions to gather information and work to categorize the comments and observations of the group.

The group agrees what is needed in metadata is driven by uses of the metadata. Some systems are only using metadata to match to an internal catalog. In other cases, metadata has broader extensive uses. Even with the same goals metadata and usage can do different things. Lack of consistency is the biggest challenge - some for good reason, some unintended.

There have also been discussions surrounding the ultimate goal the group wants to achieve. During the last meeting, the most potent statement may have been, “be realistic about the power we don’t yield.” To that end, the group did agree that collectively and individually we can be advocates for improving metadata usage and understanding across the industry. The group is reviewing the [Best Practices for Shareable Metadata](http://webservices.itcs.umich.edu/mediawiki/oaibp/index.php/IntroductionMetadataContent) document to distill ideas that will develop the Metadata 2020 mission and message in a meaningful way.

### Data Publishers and Repositories

We have found through our discussions with data publishers that they are often more advanced in their treatment of metadata gathering and communication with researchers. Members of this group have also contributed particularly interesting stories about the consequences of scant or misused of metadata to research. However, the sophistication levels within this community, when it comes to awareness of metadata quality, varies. Yet, one underlying connection is the role of stewardship for direct research outputs. Participants have offered a variety of experiences and perspectives in helpfully measuring and improving metadata, and these experiences will be shared with other groups (particularly the Publisher and Service Provider groups) to consider whether they can adopt similar approaches. They have also contributed particularly interesting stories about the consequences of scant or misuse of metadata to research.

### Funders

Early discussions with the two funders who have expressed interest in participating with Metadata 2020 have shown that there is very little awareness about metadata from funders in general, even though some are more aware of persistent identifiers and research infrastructure. Conversations have centered on how to engage the funder community; communicating the importance of metadata in scholarly communications, and the relevance it has to funders.

---

We now are in the process of gathering additional information from group members and conducting workshops, some of the information from which directing our next efforts. The Metadata 2020 core team will be working with the community group leaders to detect key areas to focus on further. Sub cross-community groups will be formed to explore these in more depth. Stay tuned for further updates on our points of focus. In the meantime, please help us by sending short summaries of metadata challenges you are facing or have faced and managed to overcome; and/or ideas involving opportunities to collaborate on developing new solutions; to me at [cdean@metadata2020.org](mailto:info@metadata2020.org).


#### About the author

Clare Dean is an independent Marketing and Communications Consultant, and the Outreach Manager for Metadata 2020. She has worked for Ashgate, John Wiley & Sons, Emerald Group Publishing, and most recently as the Director of Communications for Elementa: Science of the Anthropocene. She now serves a variety of types organizations in scholarly communications; including publishers, service providers, research institutions, and professional societies.
