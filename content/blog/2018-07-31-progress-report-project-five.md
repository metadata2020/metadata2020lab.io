+++
title = "An Update from Project 5: Shared Best Practice and Principles"
date = 2018-07-31
draft = false
tags = ["projects"]
categories = ["projects"]
banner = "/img/banners/banner_fire.jpg"
thumb = "/img/banners/thumb_fire.jpg"
author = "Jennifer Kemp"
+++

Pretty much everyone directly involved with or affected by scholarly metadata (that’s all of us by the way) is as baffled by and annoyed with its current challenges as we are hopeful and adamant about its rightful place in improving research communications.

So it’s rather daunting to be tasked with delivering to the community [Shared Best Practice and Principles](/projects/shared-practice/), the Metadata 2020 project group of which I am co-chair, along with Howard Ratner of CHORUS.


### Why best practices? Isn’t that what standards are for?

Metadata is rightly beholden to standards and we are not looking to change that, or to add another standard.

The simple (as distinct from easy) goal for our project is to provide a broad set of guiding principles and associated practices to improve how metadata is produced, delivered, communicated and used.

Standards are a bit like laws in that they are open to some interpretation. Similarly, a reasonable set of best practices cannot cover every conceivable scenario. They may be adapted (for local use if, like me, you’re partial to library parlance) and for some of us, best practices may be goals to aspire to rather than daily practice.

The important thing is that stakeholders work toward these best practices and have resources to support what fundamentally requires collaboration to succeed--quality, interoperable metadata.

To that end, our group is working on two core deliverables:

1. A list of best practice metadata elements
2. A set of guiding Best Practices and Principles

### But there are already best practices out there. Why not use those?

Indeed there are and we are very much indebted to the efforts of many other groups. Early on, we polled the community to gather a [list of existing resources](/resources/metadata-best-practices) (and will happily add to if you have suggestions).

In some ways, our work is to distill the community’s knowledge, documented and not, into a broad set of principles endorsed by a cross-section of community stakeholders.

Another early effort of the group was to crowdsource a list of metadata elements, to be provided as a matter of best practice as well as to identify those that are in need of best practice, like ‘publication date’. I suspect that sounds ridiculous to those with only a passing awareness of the issues. Isn’t publication date about as easy and straightforward as it gets? Well, no.

If you don’t believe me, stay tuned for my article “Publication Date Metadata is Bananas” in the Fall 2018 issue of Metadata Horror Stories Quarterly. When will the ‘Fall’ issue get published you ask? My point exactly.

In the meantime, that list is being used and adapted by a few of the other project groups and their feedback on it will help inform the final version. Metadata mappings? Yes please. Metadata definitions? Absolutely. I should point out -- the terminology in this post could change depending on the conclusions of the definitions group (for example: ‘element’ or ‘tag’).

Because... collaboration, people. This stuff isn’t going to get sorted out without it.

### Who do you think you are anyway?

Who are those deciding on these practices and principles? What gives us the authority to issue these guidelines? Good questions.

We’re the people that show up, that’s who. To calls, to meetings, to shared documents. We are a broad group of publishers, service providers, librarians and other stakeholders, specialists and concerned scholarly citizens. We know about this stuff, enough to know there is room for improvement, and we want to know more.

We share our time and expertise, even when we’re not sure we have enough of either. We ask questions even if they sound dumb, because we know that any valuable resource must be created, vetted, and improved by the community.

I’m not a metadata expert but I can talk for days about what metadata users want and don’t want, what publishers do and don’t get about metadata (theirs and others) as well as tell some truly bizarre metadata stories. I can’t imagine another scenario where I basically quiz people with such deep metadata knowledge and they listen and answer me (well, OK, other than my day job).

Besides, the librarian in me enjoys the slightly finger-wagging aspect of some of it.  But, seriously, the stories alone are worth the time.

How about a preview of the best practices and principles?

It’s too soon for that.  As we know from metadata -- once something is out there, it’s in the system and getting used. We'll report back in a couple of months.

You want a few personal pointers in the meantime? Sure:

1. If you don’t know, ask.
2. Lighten up. Metadata is only as ‘dry and boring’ as the people involved in it.
3. Speak up. If you care about it, you have a stake in it.
4. Listen up. If you don’t care about it, you’re probably not paying attention.
