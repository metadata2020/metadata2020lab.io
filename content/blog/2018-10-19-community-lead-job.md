+++
title = "Are you our new Community Engagement Lead?"
date = 2018-10-19
draft = false
tags = ["communities"]
categories = ["Jobs"]
banner = "/img/banners/banner_painting.jpg"
thumb = "/img/banners/thumb_painting.jpg"
author = "Clare Dean"
+++

We are seeking a part-time Community Engagement Lead to support the growing Metadata 2020 Community. As a project management and communications specialist, you will be the central contact for all community groups, projects, and sub-groups; and be the main point of contact for Metadata 2020 overall. This position is essential to the effective day-to-day running of Metadata 2020 and requires a self-starting individual with initiative and a willingness to step into a variety of tasks where needed. It is a *12-hour per week, remote role*, and you will need to be somewhat flexible with the hours you are available.

### Responsibilities include

- Develop outreach materials, including slide decks, and email communications
- Manage the website, including writing and soliciting content for the blog
- Manage Metadata 2020’s social media accounts
- Write proposals for speaking opportunities at conferences on behalf of Metadata 2020 participants
- Schedule and administer all meetings, take notes, and be prepared to update groups on the development of other projects

### Experience needed

You will have experience in managing large projects, in outreach and communications, and be a strong writer. It is important that you have a background in scholarly communications (in the library or research sectors). The ideal candidate has a knowledge of the research workflow and metadata use in research communications.

### Qualifications desired

- Degree-level education
- Experience in scholarly communications or related industry
- Evidence of community building efforts
- Demonstrated experience with managing projects and/or volunteer organizations
- Excellent communication and written English skills

Contact [info@metadata2020.org](mailto:info@metadata2020.org) with your C.V. and cover letter.
