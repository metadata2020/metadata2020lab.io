+++
title = "Thank You"
draft = false
date = "2017-07-08"
banner = '/img/backgrounds/Orange_Banner.jpg'
+++

#### Thanks for getting in touch; we'll add you to our e-newsletter.

You can also follow us on twitter [@Metadata2020](https://www.twitter.com/metadata2020) or [email Laura directly](mailto:lpaglione@metadata2020.org) if you have a suggestion or idea to discuss.
