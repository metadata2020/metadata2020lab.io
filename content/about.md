+++
title = "About"
draft = false
date = "2017-07-08"
banner = '/img/backgrounds/Orange_Banner.jpg'
style = "about"
+++

### What?

Metadata 20/20 is a collaboration that advocates richer, connected, and reusable, open metadata for all research outputs, which will advance scholarly pursuits for the benefit of society.

### How?

We aim to create awareness and resources for all who have a stake in creating and using scholarly metadata. We will demonstrate why richer metadata should be the scholarly community's top priority, how we can all evaluate ourselves and improve, and what can be achieved when we work harder, and work together.

### Why?

> Richer metadata fuels discovery and innovation.
> Connected metadata bridges the gaps between systems and communities.
> Reusable, open metadata eliminates duplication of effort.

When we settle for inadequate metadata, none of this is possible and everyone suffers as a consequence.

{{% imagebar "/img/fishes.jpg" %}}

### Who?

{{% row %}}

{{% column %}}
You! There are many opportunities for getting involved: workshopping, listening, creating, evaluating, and developing. Since October 2017 we have organized ourselves into several community groups to each define a problem statement, and come up with a value proposition for investing in richer metadata. In Q1 2018 we will report back as a broad group, and re-form into cross-community working groups around the common areas identified. This page lists the founders, community contributors, and original advisors. Please check out the [blog](/blog) for details and progress.

{{% /column %}}

{{% column %}}
The members of the core team, pushing things forward, are:

* **Ginny Hendricks**, Instigator
* **John Chodacki**, Convenor
* **Laura Paglione**, Upholder
* **Paula Reeves**, Research & creative
* **Ed Pentz**, Sponsor
{{% /column %}}
{{% /row %}}

{{% divwrap lowlight %}}
### Community Groups

These people are currently working to help define metadata challenges, barriers and opportunities for their area of scholarly communications.

{{% row %}}
{{% column %}}

#### Researchers

* **Cameron Neylon**, Curtin (Chair)
* Bethany Drehman, FASEB
* Ernesto Priego, University of London
* Eva Mendez, UC3M/OSPP
* Genevieve Early, Taylor & Francis
* John Graybeal, Stanford University
* Juan Pablo Alperin, Public Knowledge Project
* Kathryn Kaiser, UAB School of Public Health
* Kristi Holmes, Northwestern University
* L.K. Williams, Interfolio
* Mahdi Moqri, RePEc
* Mark Gahegan, University of Auckland
* Ramamohan Paturi, UC San Diego
* Ravit David, OCUL, University of Toronto Library
* Scott Wymer, Interfolio
* Stefanie Haustein, University of Ottowa
* Steve Goldenberg, Interfolio
* Steve Byford, Jisc
* Tyler Ruse, Digital Science
* Will Gregg, Simons University

#### Service Provider/Platforms and Tools

* Adrian-Tudor Pănescu, Figshare
* Bob Kasenchak, Access Innovations
* Dan Nigloschy, XML workflow solutions architect
* Dario Taraborelli, Wikimedia Foundation
* David Schott, Copyright Clearance Center
* Diane Cogan, Ringgold
* Dominic Mitchell, DOAJ
* Howard Ratner, CHOR Inc.
* Jabin White, JSTOR
* James Hardcastle, Taylor & Francis
* Jennifer Kemp, Crossref
* Jon Elwell, EBSCO
* Krishna K., Molecular Connections Pvt. Ltd.
* Lola Estelle, SPIE
* Mahdi Moqri, RePEc
* Mark Leggott, Research Data Canada
* Mary Fixler,	ProQuest
* Michael Upshall, UNSILO
* Mike Taylor, Digital Science
* Nancy Pontika, CORE
* Nannette Naught, Innovative
* Nathan Putnam, OCLC
* Neil Jefferies, Data2paper, SWORD
* Ramamohan Paturi, UC San Diego
* Ravit David, OCUL, University of Toronto Library
* Rowland Conway, Ingenta
* Stephanie Lovegrove, Silverchair
* Steve Goldenberg, Interfolio
* Scott Wymer, Interfolio
* Stephanie Dawson, Science Open


{{% /column %}}
{{% column %}}

#### Funders

* Ross Mounce, Arcadia Fund


#### Publishers

* **Daniel Shanahan**, Cochrane (Chair)
* Fiona Counsell, Taylor & Francis
* Christina Gifford, Elsevier
* Christina Hoppermann, Springer Nature
* Concetta La Spada, Cambridge University Press
* Cyrill Martin, Karger Publishers
* Ed Pentz, Crossref
* Ed Moore, SAGE Publishing
* Ginny Hendricks, Crossref
* Helen King, British Medical Journal
* Jim Swainston, Emerald Group Publishing
* Jonathan Glasspool, Bloomsbury Publishing Plc
* Julie Zhu, IEEE
* László Simon-Nanko, Mohr Siebeck
* Melissa Harrison, eLife
* Michael Evans,	F1000
* Nicholas Everitt, Taylor & Francis
* Paul Dlug, American Physical Society
* Peter Strickland, IUCr Journals
* Phil Shaw, IOP
* Sarah Whalen, AAAS
* Stacy Masucci, Elsevier
* Teresa Lewis, Cambridge University Press
* Will Wilcox, John Wiley & Sons


#### Librarians

* **Juliane Schneider**, Harvard Catalyst (Chair)
* Christopher Erdmann, North Carolina State University
* Dana Jemison, California Digital Library
* Ebe Kartus, University of New England
* Eva Mendez, Universidad Carlos III de Madrid
* Fiona Bradley, Research Libraries UK
* Helen Williams, LSE Library, The London School of Economics and Political Science
* Kaci Resau, Washington & Lee University
* Kathryn Sullivan, University of Manchester
* Laura Cagnazzo, Abertay University
* Maria Johnsson, Lund University
* Nannette Naught, Innovative
* Pam White, Hawaii Pacific University
* Ravit David, OCUL, University of Toronto Library
* Scott Plutchak, University of Alabama at Birmingham (retired)
* Tammy Moorse,	Hamilton Public Library

#### Data Publishers and Repositories

* **John Chodacki**, CDL and DataCite (Chair)
* Adrian Price,	SUND/SCIENCE Bibliotek
* Barbara Chen, Modern Language Association
* Jennifer Lin, Crossref
* Scott Plutchak, University of Alabama at Birmingham (retired)
* Ted Habermann, HDF Group

{{% /column %}}
{{% /row %}}
{{% /divwrap %}}

### Advisors

These are the folks who were interviewed originally and while the current activity lies with the community groups, these people have a running interest in the goals of Metadata 20/20.

{{% row %}}
{{% column %}}
* **John Chodacki**, California Digital Library (Chair)
* Alice Meadows, ORCID
* Cameron Neylon, Curtin University
* Carly Strasser, Moore Foundation
* Caroline Sutton, Taylor & Francis
* Dario Taraborelli, Wikimedia Foundation
* Eva Mendez Rodriguez, Universidad Carlos III de Madrid
* Ed Pentz, Crossref
* Genevieve Early, Taylor & Francis
* Ginny Hendricks, Crossref
* Juan Pablo Alperin, Public Knowledge Project
{{% /column %}}

{{% column %}}
* Kristen Ratan, Coko Foundation
* Laure Haak, ORCID
* Mark Patterson, eLife
* Mike Taylor, Digital Science
* Natalia Manola, OpenAIRE
* Patricia Cruse, DataCite
* Paul Dlug, American Physical Society
* Roy Tennant, OCLC
* Scott Plutchak, University of Alabama
* Stefanie Haustein, University of Ottawa
* Steve Byford, JISC
{{% /column %}}
{{% /row %}}

_Note that listing an individual contributor on this page doesn't necessarily mean an official endorsement from their employer._

---
Please [email us](mailto:info@metadata2020.org) if you'd like to know more.
