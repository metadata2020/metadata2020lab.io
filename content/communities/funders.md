+++
title = "Funders"
draft = false
date = "2018-02-06"
banner = '/img/banners/funders.jpg'
style = "communities"
+++

### Summary problem statement

Funders are seen as the core solution to the problems surrounding interconnectedness.  However, the reality is that most funding organizations are conservative organizations that are not tech savvy.  They lack the context of the issues we are discussing to be able to truly address them.  Their focus is on portfolio management and success metrics and not on the quality or discoverability of their funded research.  While this is evolving, we need to find ways to excite funders to the opportunity.

### Challenges

* Many funders are not even aware of ‘metadata’
* Funders are focused on funding, and not on scholarly communications (publishing, discoverability, citations, etc.)
* Funders are not communications-focused, and therefore not concerned with communicating stories around their funding
* Many funders in the Research areas are run by individuals or groups that are traditionalists and lack technical skills or knowledge.
* Even progressive funders are still focused on policy changes (OA, data sharing, etc.) and not on technical infrastructure issues.

### Opportunities

* Using data visualizations as a way of bridging tech novices.  Tell them a story that shows the power of metadata, using metadata.
* Make sure to draw parallels/connections between enacting new policies and the required metadata.
* Metrics.  Funders love metrics. Show that citations go up, etc.
* With the advent of Chronos and Funder collaborations, we see more communication within funders and this could mean more possibility of them educating each other.
* Publishers are willing to collect Funder metadata.  So there is an opening for funders to join the dialogue.
* Funders have the means to help contribute to solving costly metadata problems

### What exactly can Funders do?

* Require normalized/clean funder information for easier auditing of investments
* Collect metadata themselves.  Clean it up.  Push clean metadata downstream/upstream.
* Encourage researchers, publishers and others to make clean metadata freely available.
* Create a grant application question about good examples in the publishing, data, and institution spaces (those that care about these issues) and getting that ranking in the decision process for grant funding.

### Vision Statement

Funders have easy, open access to metrics around their research and the research itself is optimized for broadest possible reach and impact.  Basically, funders get more for their money and their research get exposure with less friction.

### Group participants

* Ross Mounce, Arcadia Fund


### Projects
The Funders Community Group are involved with: TBD February 2018

---
Please [email us](mailto:info@metadata2020.org) if you'd like to know more.
