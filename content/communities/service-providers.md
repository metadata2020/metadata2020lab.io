+++
title = "Service Providers/Platforms & Tools"
draft = false
date = "2018-02-06"
banner = '/img/banners/service-pro.jpg'
style = "communities"
+++


### Summary problem statement

Service providers, and providers of platforms and tools face a host of interoperability challenges. Inconsistent metadata vocabulary are a primary concern; and a lack of community standards results in major usability challenges downstream. A deficient understanding of the variety of uses and therefore needs of metadata among librarians, publishers, and the entire data supply chain is the root of many systemic issues surrounding metadata challenges for the community.

### Challenges

* There is a lack of consistency of metadata schema, requirements, and entry; and because we lack well-followed community standards, there is inconsistency in handling items such as dates, special characters and metadata vocabulary
* There is a lack of understanding from metadata creators about which metadata fields are necessary to consumers of content; and lack of understanding of the context of metadata. Metadata creators make assumptions that don’t travel with the data.
* Inadequate understanding, throughout the supply chain, of the context of metadata and its varied uses lead to improper choices of schema that result in inadequate metadata or loss of metadata.
* Records and metadata elements can disappear in the supply chain without transparency as to where or why
* Metadata is often out-of-date and updates are not supplied by the metadata creators.

### Opportunities

* Aim at increased consistency by collaborating with other communities to form and distribute best practice guidelines.
* Increase understanding and uses of metadata by collaborating with other communities to map metadata, build awareness about the myriad of uses of metadata, and detect where there are inefficiencies, or where metadata and records may be lost.
* Build awareness of multiple uses and needs of metadata among publishers, librarians etc., by development of use cases, demonstration of metadata mapping, collaboration and community engagement
* Develop and integrate metadata evaluation tools for metadata creators in consultation with the wider scholarly communications community
* Develop and integrate new metadata tools to increase interoperability across systems and avoid duplication of metadata entry

### What exactly can Service Providers / Platforms and Tools do?

* Work alongside publishers, librarians, data publishers/ repositories, and funders to map the metadata journey
* Define best practices and core principles and disseminate this to the wider scholarly communications community
* Collaborate with publishers, data publishers and repositories, libraries, funders and researchers to develop evaluative tools for improved quality
* Gather, publish and disseminate use cases to demonstrate the uses and misuses of metadata from the service provider perspective.
* Consider how service providers, platforms and tools can potentially collaborate to form new systems to avoid the duplication of metadata entry; which will ultimately encourage fuller and more accurate metadata entry per object.

### Vision Statement & Value Proposition

We will work from within the scholarly data supply chain to facilitate:

* disseminating messaging about the value and uses of metadata to the wider community
* collaborative mapping of the metadata journey
* creating a consistent metadata vocabulary
* defining and refining best practices
* developing and integrating new evaluative tools for improved quality and quality control of metadata
* addressing major interoperability challenges in assessing and communicating inconsistencies and breakages to data publishers, librarians, and publishers

### Group participants

{{% row %}}
{{% column %}}

* Adrian-Tudor Pănescu, Figshare
* Dan Nigloschy, XML workflow solutions architect
* Dario Taraborelli, Wikimedia Foundation
* David Schott, Copyright Clearance Center
* Diane Cogan, Ringgold
* Dominic Mitchell, DOAJ
* Howard Ratner, CHOR Inc.
* Jabin White, JSTOR
* Jennifer Kemp, Crossref
* Jon Elwell, EBSCO
* Krishna K., Molecular Connections Pvt. Ltd.
* Lola Estelle, SPIE
* Mahdi Moqri, RePEc
* Mark Leggott, Research Data Canada
* Mary Fixler,	ProQuest
* James Hardcastle, Taylor & Francis

{{% /column %}}
{{% column %}}

* Michael Upshall, UNSILO
* Mike Taylor, Digital Science
* Nancy Pontika, CORE
* Nannette Naught, Innovative
* Nathan Putnam, OCLC
* Neil Jefferies, Data2paper, SWORD
* Ramamohan Paturi, UC San Diego
* Ravit David, OCUL, University of Toronto Library
* Rowland Conway, Ingenta
* Stephanie Lovegrove, Silverchair
* Steve Goldenberg, Interfolio
* Scott Wymer, Interfolio
* Stephanie Dawson, Science Open

{{% /column %}}
{{% /row %}}

### Projects
The Service Provider/Platforms and Tools Community Group are involved with: TBD February 2018


---
Please [email us](mailto:info@metadata2020.org) if you'd like to know more.
