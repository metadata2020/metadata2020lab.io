+++
title = "Researchers"
draft = false
date = "2018-02-06"
banner = '/img/banners/researchers.jpg'
style = "communities"
+++

### Summary problem statement

Researchers have a major issue with time. Metadata entry upon submission of research takes time, and this metadata is often required to be entered multiple times. Streamlining is needed. Researchers in different fields have different metadata needs and ways of talking about metadata. There is also a lack of knowledge surrounding the importance of complete and accurate metadata, and the value and uses of that metadata upstream in the research product life cycle.

### Challenges

* Duplication of metadata entry efforts across multiple systems requires extensive time
* Researchers do not understand the ramifications of their actions; they lack the understanding that their contributions in submission systems are mapped directly into the long-term metadata records
* Institutions require deposit of research and data in repositories which further increases workload
* There is a lack of meaningful, acknowledged incentive to understanding why getting and maintaining good metadata creates issues. The authors of the metadata are not the users. There needs to be a stronger story surrounding the link between complete metadata and discoverability.
* There are many frustrations with submission systems - why don’t we have more automation to extract and pre-populate metadata for them etc.?

### Opportunities

* Researchers are well-positioned to respond to mandated funder metadata requirements
* Researchers respond well to evidence. Strengthening the story between complete metadata and discoverability, will likely result in more complete metadata.
* Researchers respond to evaluation systems, and so will likely be responsive to metadata completeness systems upon submission
* ORCID is being adopted, and continued adoption will increase interoperability, cleanliness and the sharing of metadata
* Help publishers and submission systems providers reimagine a submission system that is designed for maximal information gathering and interoperability, with minimal effort on behalf of the researcher (and publisher staff)

### What exactly can the Researcher community do?

* Be available for feedback to groups trying to correctly assess researcher pain points and downstream needs
* Promote, and get in front of researchers, better information on the importance, value and use of metadata throughout the research product lifecycle.  Emphasizing the value to the researcher.
* Be available to test pilot systems, metadata vocabulary, and messaging developed by other communities in scholarly communications
* Obtain, use, and evangelize ORCID iDs, requiring them as part of the submission process.
* Design systems of metadata management for researchers that minimize or eliminate the need for submission systems. Ideally we can maintain shareable metadata in an ongoing manner, automatically, then minimize the additional action needed (ie, a single“yes” click) so researchers use near-zero time to respond to requests.

### Vision Statement

Other communities in scholarly communications are amenable to helping researchers understand the importance of metadata; and streamline the vocabulary, messaging, and systems to make it easier for researchers to deposit full metadata. As other communities do so, researchers would benefit from contributing to the information gathering, developing, and testing, so that solutions developed for them are relevant and optimized for their needs.

### Group participants

* **Cameron Neylon**, Curtin (Chair)
* Bethany Drehman, FASEB
* Ernesto Priego, University of London
* Eva Mendez, UC3M/OSPP
* Juan Pablo Alperin, Public Knowledge Project
* L.K. Williams, Interfolio
* Mahdi Moqri, RePEc
* Mark Gahegan, University of Auckland
* Ramamohan Paturi, UC San Diego
* Ravit David, OCUL, University of Toronto Library
* Stefanie Haustein, University of Ottowa
* Scott Wymer, Interfolio

### Projects
The Researchers Community Group are involved with: TBD February 2018

---
Please [email us](mailto:info@metadata2020.org) if you'd like to know more.
