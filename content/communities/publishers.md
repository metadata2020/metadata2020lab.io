+++
title = "Publishers"
draft = false
date = "2018-02-06"
banner = '/img/banners/publishers.jpg'
style = "communities"
+++

### Summary problem statement

Publishers of all content types (journals, books, conference papers, “data” etc.) and from all disciplines are in a unique position with regard to metadata. They collect, create, vet, and disseminate metadata for multiple purposes in line with a variety of standards. At the same time, publishers are challenged with establishing streamlined, efficient workflows for metadata management due to due to siloed expertise and unclear prioritization within organizations and across the community as a whole. They face a major obstacle in the procurement of complete and accurate metadata, and struggle with balancing the desire to collect metadata upon submission, with the need to minimize the burden on authors when submitting a manuscript. Publishers are keen to map their metadata workflows; find new efficiencies by working with the other communities in scholarly communications (particularly service providers/platforms and tools, and librarians); and create a consistent vocabulary and messaging around metadata for communication with researchers.

### Challenges

* The cost/benefit of investing in metadata management is not always evident. Getting metadata right (tagging items themselves etc.) is costly for publishers., Requirements continually evolve and current systems are not able to incorporate changes quickly enough.
* The community lacks a cohesive metadata distribution model. There are organizations such as Crossref, DataCite, Europe PMC that act as metadata hubs but they too are siloed (e.g. by content type, member community, subject field, schema). Interoperability is a key concern.
* The author community does not have sufficient information about the reasons for supplying full metadata, and they often have to duplicate the metadata entry in multiple places. The information required may not necessarily be contained within their Word/LaTeX document, and many see completing a form in full as an inconvenience.
* Metadata versioning is a problem; metadata goes through a number of manipulations which create challenges around version of record and trustworthiness.
* Vendor submission systems have certain requirements that publishers cannot control.
* Attaching appropriate/new metadata to previously published material is a huge challenge (insurmountable for many publisher archives).

### Opportunities

* Potential for collaboration to define a consistent vocabulary around metadata for researchers (in collaboration with librarians, data providers, funders, and researchers.)
* Publishers have the potential to work with submission system platform providers to facilitate more efficient data capture, one-time metadata entry and the possibility of metadata evaluation tools for researchers upon submission.
* Develop business cases for improved metadata that directly results in optimized discoverability; and examples of, or pilots for incentive structure for optimized metadata internal to organizations.
* Thoroughly research customer needs and expectations regarding information required (e.g. is a rights statement useful?)
* After a thorough metadata mapping, collectively communicate with service providers to define interoperability requirements.

### What exactly can Publishers do?

* Work with other groups to help define a consistent vocabulary around metadata for researchers.
* Collaborate to create messaging, use cases, etc., to explain the importance of metadata to authors, editors, internal staff, and suppliers (consulting with the researcher group for accuracy).
* Contribute to defining best practices for publishers.
* Interoperability discussions with service providers/platforms and tools, data suppliers, and librarians - what are the key priorities/major issues that need to be addressed? Can the publisher group come to a consensus on this?
* Consider the integration of metadata evaluation tools into submission process, and elsewhere for production staff etc.

### Vision Statement

As scholarly publishers, we aim to understand, plan, evaluate, and communicate the importance of richer, connected, and reusable, open metadata. Through collectively lobbying for better quality metadata--internally and with our partners--we will maximize the meaningfulness of the information we provide, and have a positive impact on the scholarly community that we serve.

### Group participants

{{% row %}}
{{% column %}}


* **Daniel Shanahan**, Cochrane (Chair)
* Angela Dappert, Springer Nature
* Caroline Sutton, Taylor & Francis
* Christina Gifford, Elsevier
* Christina Hoppermann, Springer Nature
* Concetta La Spada, Cambridge University Press
* Cyrill Martin, Karger Publishers
* Ed Moore, SAGE Publishing
* Ed Pentz, Crossref
* Ginny Hendricks, Crossref
* Helen King, British Medical Journal
* Henning Schoenenberger, Springer Nature

{{% /column %}}
{{% column %}}

* Jim Swainston, Emerald Group Publishing
* Jonathan Glasspool, Bloomsbury Publishing Plc
* Julie Zhu, IEEE
* László Simon-Nanko, Mohr Siebeck
* Melissa Harrison, eLife
* Michael Evans,	F1000
* Nicholas Everitt, Taylor & Francis
* Paul Dlug, American Physical Society
* Peter Strickland, IUCr Journals
* Phil Shaw, IOP
* Stacy Masucci, Elsevier
* Teresa Lewis, Cambridge University Press
* Will Wilcox, John Wiley & Sons

{{% /column %}}
{{% /row %}}

### Projects
The Publishers Community Group are involved with: TBD February 2018

---
Please [email us](mailto:info@metadata2020.org) if you'd like to know more.
