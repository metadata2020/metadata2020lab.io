+++
title = "Librarians"
draft = false
date = "2018-02-06"
banner = '/img/banners/librarians.jpg'
style = "communities"
+++

### Summary problem statement

There are a variety of interoperability and standards-related issues that prohibit efficient work with metadata; but a larger challenge is the culture surrounding metadata in the library; which is overly focused on technical details and the minutiae of standards driven by a large amount of legacy data shared between the system vendors and libraries; and limited in ability to think about the bigger picture of metadata and how a different system may be more efficient.

There is also a lot of knowledge surrounding metadata use and unintended consequences of “dirty data” in the librarian community, although this knowledge is not always well-distributed in and out of the library. Librarians face challenges when communicating with researchers about metadata; when making use of metadata created by others; and when applying metadata to digital objects.

### Challenges

* Financially, there is a lot invested in proprietary systems or siloed metadata standards
* There are a variety of different uses of the word ‘metadata’ and surrounding vocabulary. Librarians use it in a particular way, but this is not in line with the language surrounding information from different researcher groups.
* Libraries are overly focused on legacy systems and workflows that prevent new efforts getting very far. Librarians try to fit new models into legacy systems rather than developing new systems to support new models.
* There are major interoperability challenges, resulting in a lot of duplication of data entry for use with different systems.
* Researchers in different disciplines use different language surrounding metadata and have built their own systems. The challenge is how to introduce enough consistency.
* There is an ongoing challenge of extracting enough metadata for discovery but not overburdening the metadata providers.

### Opportunities

* With extensive knowledge about metadata and its applications, librarians are in an excellent place to repurpose metadata expertise elsewhere in the scholarly communications chain, working with researchers when needed; and directly with and for publishers.
* In many institutions, librarians have built well established, collaborative relationships with researchers, providing a foundation for metadata work.
* Librarians can collaborate with researchers, service providers, and publishers to contribute to a common metadata vocabulary and consistent communication plan
* A more widespread adoption of persistent identifiers in libraries helps advancement in authority work and disambiguation of names and institutions.
* Librarians and researchers will likely be receptive to use cases of easy systems implementation for heightened interoperability (e.g. ORCID).
* The community responds well to principles and best practice outlines so can meaningfully contribute and respond to best practice guidelines

### What exactly can Librarians do?

* Collaborate with publishers, service providers, data publishers and data analysts to achieve a comprehensive metadata map across scholarly communications and from this establish metadata principles and best practice documentation
* Serve in an advisory capacity for the development of new metadata evaluation tools, and systems to streamline interoperability
* Identify prioritized areas for metadata development and integration in the new research paradigm, e.g. metadata for research data.
* Collaborate to form a new metadata vocabulary, working with researchers to define  and expand the nuances per field
* Work with researchers to disseminate new vocabulary and messaging around metadata
* In concert with service providers and platforms and tools, data publishers and repositories, and publishers, re-imagine a fully interoperable and efficient system of creating, vetting, sharing, and reusing metadata
* Develop and share stories and use cases to share among the community to inspire action to optimize efficiency and accuracy
* Work towards open, de-siloed metadata that promotes cross-platform interoperability and sharing

### Vision Statement

Librarians are in a unique position of having considerable knowledge of metadata creation and use, but without the vision or resources to reimagine a more efficient system. The community’s collaboration with other scholarly communications communities is vital to be able to use expertise for the collective good, and gradually shift the culture from one of stasis to one flexible and open enough to allow collaboration with the motivation to continually improve; adopting new models rather than trying to further secure legacy systems.

### Group participants

* **Juliane Schneider**, Harvard Catalyst (Chair)
* Christopher Erdmann, North Carolina State University
* Dana Jemison, California Digital Library
* Ebe Kartus, University of New England
* Eva Mendez, Universidad Carlos III de Madrid
* Fiona Bradley, Research Libraries UK
* Helen Williams, LSE Library, The London School of Economics and Political Science
* Jennifer Kemp, Crossref
* Kaci Resau, Washington & Lee University
* Kathryn Sullivan, University of Manchester
* Laura Cagnazzo, Abertay University
* Maria Johnsson, Lund University
* Nannette Naught, Innovative
* Pam White, Hawaii Pacific University
* Ravit David, OCUL, University of Toronto Library
* Scott Plutchak, University of Alabama at Birmingham (retired)
* Tammy Moorse,	Hamilton Public Library

### Projects
The Librarians Community Group are involved with: TBD February 2018

---
Please [email us](mailto:info@metadata2020.org) if you'd like to know more.
