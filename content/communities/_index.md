+++
title = "Communities"
draft = false
date = "2018-01-16"
banner = '/img/banners/communities.jpg'
style = "communities"
+++

The Metadata 20/20 Community Groups defined the metadata challenges, barriers and opportunities for their area of scholarly communications. As metadata ambassadors, they collaborate to expand discussions to peer organizations, and find solutions to common community-specific problems.

These are the summary problem statement from each community group.

### Researchers

> Researchers have a major issue with time. Metadata entry upon submission of research takes time, and this metadata is often required to be entered multiple times. Streamlining is needed. Researchers in different fields have different metadata needs and ways of talking about metadata. There is also a lack of knowledge surrounding the importance of complete and accurate metadata, and the value and uses of that metadata upstream in the research product life cycle.

##### [Read researcher challenges and possible solutions](/communities/researchers/)

### Publishers

> Publishers of all content types (journals, books, conference papers, “data” etc.) and from all disciplines are in a unique position with regard to metadata. They collect, create, vet, and disseminate metadata for multiple purposes in line with a variety of standards. At the same time, publishers are challenged with establishing streamlined, efficient workflows for metadata management due to due to siloed expertise and unclear prioritization within organizations and across the community as a whole. They face a major obstacle in the procurement of complete and accurate metadata, and struggle with balancing the desire to collect metadata upon submission, with the need to minimize the burden on authors when submitting a manuscript. Publishers are keen to map their metadata workflows; find new efficiencies by working with the other communities in scholarly communications (particularly service providers/platforms and tools, and librarians); and create a consistent vocabulary and messaging around metadata for communication with researchers.

##### [Read publisher challenges and possible solutions](/communities/publishers/)

### Librarians

> There are a variety of interoperability and standards-related issues that prohibit efficient work with metadata; but a larger challenge is the culture surrounding metadata in the library; which is overly focused on technical details and the minutiae of standards driven by a large amount of legacy data shared between the system vendors and libraries; and limited in ability to think about the bigger picture of metadata and how a different system may be more efficient.
>
> There is also a lot of knowledge surrounding metadata use and unintended consequences of “dirty data” in the librarian community, although this knowledge is not always well-distributed in and out of the library. Librarians face challenges when communicating with researchers about metadata; when making use of metadata created by others; and when applying metadata to digital objects.

##### [Read librarian challenges and possible solutions](/communities/librarians/)


### Data Publishers & Repositories

> While Data Publishers and Repositories work directly with researchers to create and improve metadata, our community routinely struggles with adoption. This creates a tension between quantity of deposits versus the quality of the metadata collected. For those data repos that are successful, there is still a struggle to deal with inconsistent information across repository datasets. Many of these issues are due to the position of the data repositories within researcher workflows. However, there are other significant hurdles including use of inconsistent metadata vocabularies, lack of tools, and lack of integrated metadata evaluations and guidance.

##### [Read data publisher and reposotory challenges and possible solutions](/communities/data-publishers-and-repositories/)

### Services, Platforms & Tools

> Service providers, and providers of platforms and tools face a host of interoperability challenges. Inconsistent metadata vocabulary are a primary concern; and a lack of community standards results in major usability challenges downstream. A deficient understanding of the variety of uses and therefore needs of metadata among librarians, publishers, and the entire data supply chain is the root of many systemic issues surrounding metadata challenges for the community.

##### [Read services, platforms and tools challenges and possible solutions](/communities/service-providers/)

### Funders

> Funders are seen as the core solution to the problems surrounding interconnectedness. However, the reality is that most funding organizations are conservative organizations that are not tech savvy. They lack the context of the issues we are discussing to be able to truly address them. Their focus is on portfolio management and success metrics and not on the quality or discoverability of their funded research. While this is evolving, we need to find ways to excite funders to the opportunity.

##### [Read funder challenges and possible solutions](/communities/funders/)

---
If you are interested in joining any of these groups, or communicating with them directly, please email [info@metadata2020.org](mailto:info@metadata2020.org).
