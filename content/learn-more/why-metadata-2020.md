+++
title = "Why Metadata 20/20"
draft = false
date = "2020-11-19"
type = "learn-more"
banner = '/img/learnmore/topbar.svg'
+++

Metadata 20/20 was conceived in late 2017 with the goal of rallying and
supporting the community around the critical issue of sharing fisher metadata
for research communications. It’s mission? To advocate for richer, connected
and reusable metadata for all outputs because:

* Richer metadata fuels discoverability and innovation
* Connected metadata bridges the gaps between systems and communities
* Reusable metadata eliminates duplication of effort

Participants in Metadata 20/20's collaborative work recognized that when we settle for inadequate metadata, none of these results are possible, and everyone using research outputs will suffer as a consequence