+++
title = "Foundation"
draft = false
date = "2020-11-19"
banner = '/img/learnmore/topbar.svg'
subhead = 'Phase 0'
image = '/img/learnmore_phase00.svg'
layout = "phase"
linktext = "Read the Foundation chapter in 'About Metadata 20/20'"
linkurl = "https://docs.google.com/document/d/1JQdNsrTLlYuKhyVUu_NuYZCPnj1IJNIRDYprWv7rSfM/edit#heading=h.ylxhemtap5q9"
navid = 'foundation'
+++

{{% learn-more/top-section %}}

> Most people wouldn't think: 'Well, if we can fix this metadata we can find a cure for cancer.' If we can find a way to connect those dots that would be huge. Nobody is asking: What is the cost to society?

Conceived in October 2017, Metadata 20/20 was founded to advocate for richer, connected
and reusable metadata. With significant metadata efforts already underway, the project sought to rally and support the community around the critical issue of sharing richer metadata for research communications because

* Richer metadata fuels discoverability and innovation
* Connected metadata bridges the gaps between systems and communities
* Reusable metadata eliminates duplication of effort

Participants in Metadata 20/20's collaborative work  recognized that when we settle for inadequate metadata, none of these results are possible, and everyone using research outputs will suffer as a consequence.

#### The Foundation Work

Our Foundational work started with a small group that had been thinking about the critical importance of metadata. This sparked the involvement of a broad community that helped to define a framework for what became the international, community-driven initiative of Metadata 20/20. This page shares some of the highlights, with details provided in the report linked below.

<script src="https://fast.wistia.com/embed/medias/hj995m30aa.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_hj995m30aa videoFoam=true" style="height:100%;position:relative;width:100%"><div class="wistia_swatch" style="height:100%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:100%;"><img src="https://fast.wistia.com/embed/medias/hj995m30aa/swatch" style="filter:blur(5px);height:100%;object-fit:contain;width:100%;" alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;" /></div></div></div></div>


<!--{{% learn-more/link %}}-->

{{% /learn-more/top-section %}}

{{% learn-more/sections "phase_0" %}}

### The Initial Metadata 2020 Phased Goals

As a result of this foundational work, the advisory group established a set of phased goals.

> We aim to create awareness and resources for all who have a stake in creating and using scholarly metadata. We will demonstrate why richer metadata should be the scholarly community’s top priority, how we can all evaluate ourselves and improve, and what can be achieved when we work harder, and work together.

By the end of 2020, we will

1. Articulate the value of metadata
    * Best practice framework (metadata maturity model, upstream/downstream)
    * Business cases
2. Solicit audience-specific views
    * Values for different communities
    * Stories (qualitative)
    * Research (quantitative)
3. Identify common metadata problems across the community (e.g., title, author, institution, funder, etc.)
4. Identify audience-specific metadata problems
5. Connect it all, pull it all together and assess required collective action. Make it easier and more efficient.  “If I do X, Y will happen.”



<p>&nbsp;</p>