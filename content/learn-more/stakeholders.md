+++
title = "Stakeholders"
draft = false
date = "2020-11-19"
banner = '/img/learnmore/topbar.svg'
subhead = 'Phase 1'
image = '/img/learnmore_phase01.svg'
layout = "phase"
linktext = "Read the Communities chapter in 'About Metadata 20/20'"
linkurl = "https://docs.google.com/document/d/1JQdNsrTLlYuKhyVUu_NuYZCPnj1IJNIRDYprWv7rSfM/edit#heading=h.ylxhemtap5q9"
navid = 'stakeholders'
+++


{{% learn-more/top-section %}}
From its start, Metadata 20/20 sought to engage publishers,
aggregators, service providers, librarians, funders and researchers
to participate in the campaign by committing to improving
the quality and interoperability of their metadata. The first step of
this journey was to identify community members that could help
articulate the metadata challenges, barriers and opportunities for
their area of scholarly communications. This work helped define
a problem statement with a set of metadata challenges and
opportunities for each group, and led to a vision statement for each
of these communities of what success would look like if our
metadata goals were achieved.
<!--{{% learn-more/link %}}-->
{{% /learn-more/top-section %}}






{{% learn-more/sections "phase_1" %}}
