+++
title = "Outcomes"
draft = false
date = "2020-11-19"
banner = '/img/learnmore/topbar.svg'
subhead = 'Phase 3'
image = '/img/learnmore_phase03.svg'
layout = "phase"
linktext = "Read the Outcomes chapter in 'About Metadata 20/20'"
linkurl = "https://docs.google.com/document/d/1JQdNsrTLlYuKhyVUu_NuYZCPnj1IJNIRDYprWv7rSfM/edit#heading=h.ylxhemtap5q9"
+++

{{% learn-more/top-section %}}

outcomes text here

{{% learn-more/link %}}
{{% /learn-more/top-section %}}


{{% learn-more/sections "phase_3" %}}