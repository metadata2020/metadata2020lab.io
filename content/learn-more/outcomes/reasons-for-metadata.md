+++
title = "Reasons for metadata"
draft = false
date = "2020-11-19"
type = "outcomes"
banner = '/img/learnmore/topbar.svg'
subhead = 'Phase 3. Outcomes'
image = '/img/learnmore_phase03.svg'
layout = "/learnmore/phase"
linktext = "reasons"
+++

<h4>Reasons for metadata</h4>

During Metadata 20/20's first user workshop, we asked participants what they want to do that can be enabled by metadata, and why they feel this action is important. Each metadata reason uses the statement format As a _____ I want to ______ so I can ____.

<hr/>

{{< do-more/i-want-to role="Author" want="know all research being conducted on a given topic" why="maximize the effectiveness of my own research choices" >}}

{{< do-more/i-want-to role="Educator" want="hear more discussion about scholarly communications for undergraduates/general public" why="teach users and students how these issues affect them" >}}

{{< do-more/i-want-to role="Indexer in the field of religion" want="collect significant religious conversations from the global south" why="teach users and students how these issues affect them" >}}

{{< do-more/i-want-to role="IR Manager" want="be able to capture all the scholarly/creative output of the university " why="grow the IR and make it a vital part of campus culture" >}}

{{< do-more/i-want-to role="IR Manager" want="be able to auto-ingest content/metadata from other platforms" why="be more efficient and productive" >}}

{{< do-more/i-want-to role="IR Manager" want="be able to post OA versions of all our faculty's publications" why="raise the profile of the university and share knowledge freely" >}}

{{< do-more/i-want-to role="Librarian" want="influence faculty practice/engagement with OA" why="build better understanding and awareness" >}}

{{< do-more/i-want-to role="Librarian" want="develop a course/workshop about writing lay summaries" why="I can help researcher disseminate their research to the public" >}}

{{< do-more/i-want-to role="Librarian" want="make all information freely available" why="empower people without financial resources" >}}

{{< do-more/i-want-to role="Librarian" want="incorporate scientific writing instruction in medical education" why="help make research dissemination more efficient" >}}

{{< do-more/i-want-to role="Librarian" want="learn Python or R" why="be more independent and not depend on our data scientists" >}}

{{< do-more/i-want-to role="Librarian" want="spend more time interning at a publisher and/or funding agency" why="gain a better understanding of the whole picture" >}}

{{< do-more/i-want-to role="Biomedical Librarian" want="distinguish authorship from 'group' authorship" why="use this in research assessment" >}}

{{< do-more/i-want-to role="Scholarly Communications Librarian" want="an efficient/reliable way to access publisher policy" why="include works in the IR quicker" >}}

{{< do-more/i-want-to role="Scholarly Communications Librarian" want="metadata to be entirely interoperable without the need of human intervention" why="make descriptive work faster" >}}

{{< do-more/i-want-to role="Scholarly Communications Librarian" want="for someone to lay out the entire scholarly communications process with links to all tools and players" why="so that I am exposed to everything I need to do my job" >}}

{{< do-more/i-want-to role="Scholarly Communications Librarian" want="find easy non-jargon training materials about scholarly communications technology" why="better explain to faculty" >}}

{{< do-more/i-want-to role="Scholarly Communications Librarian" want="easily find all the research from my institution" why="include it in the IR" >}}

{{< do-more/i-want-to role="Scholarly Communications Librarian" want="have more training opportunities like FSCI" why="understand these processes more easily and transfer that knowledge to others" >}}

{{< do-more/i-want-to role="Library Director" want="greater adoption of researcher IDs" why="help my staff spend less time on manually searching for author publications" >}}

{{< do-more/i-want-to role="Publisher" want="know all the books in press on a given topic" why="decide on book acceptances" >}}

{{< do-more/i-want-to role="Publishing Editor" want="be able to expel the stigma of open access content as being less robust" why="develop an OA platform" >}}

{{< do-more/i-want-to role="Publishing Editor" want="be able to put together an institutional outreach program to instruct authors in putting together a strong manuscript" why="cut down on editorial TAT[?] increasing submissions and publications" >}}

{{< do-more/i-want-to role="Publishing Editor" want="cut down on editorial TAT(?) increasing submissions and publications" why="reduce turn around times on each journal and increase submissions to publications" >}}

{{< do-more/i-want-to role="Research Assistant" want="have a 'researcher' metadata field associated with publications" why="get credit for my work" >}}

{{< do-more/i-want-to role="Scholar" want="find all literature on a given topic" why="do my work" >}}