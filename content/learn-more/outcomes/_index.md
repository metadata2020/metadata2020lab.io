+++
title = "Outcomes"
draft = false
date = "2020-11-19"
banner = '/img/learnmore/topbar.svg'
subhead = 'Phase 3'
image = '/img/learnmore_phase03.svg'
layout = "phase"
linktext = "Read the Outcomes chapter in 'About Metadata 20/20'"
linkurl = "https://docs.google.com/document/d/1JQdNsrTLlYuKhyVUu_NuYZCPnj1IJNIRDYprWv7rSfM/edit#heading=h.ylxhemtap5q9"
navid='outcomes'
+++

{{% learn-more/top-section %}}

During the Metadata 20/20 initiative, the community created several materials and resources that add to the body of knowledge about metadata, its use, and how to support it. Here we list these outcomes.

<!--{{% learn-more/link %}}-->
{{% /learn-more/top-section %}}


{{% learn-more/sections "phase_3" %}}