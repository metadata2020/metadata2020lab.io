+++
title = "Your Turn"
draft = false
date = "2020-11-19"
banner = '/img/learnmore/topbar.svg'
subhead = 'Phase 4'
image = '/img/learnmore_phase04.svg'
layout = "phase"
linktext = "Read the details of what you can do"
linkurl = "/do-more/"
navid = "your-turn"
+++

{{% learn-more/top-section %}}

The next steps of this effort are up to YOU! The work done to date provides a catalyst for the work ahead, and a backdrop for the contributions that you will make to this metadata movement. We invite you to learn more about the contributions made so far, and act to ensure that we can realize the benefits of richer metadata.

{{% learn-more/link %}} 
{{% /learn-more/top-section %}}


<!--- There is no detail on this page
      {{% learn-more/sections "phase_4" %}} -->