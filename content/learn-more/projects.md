+++
title = "Projects"
draft = false
date = "2020-11-19"
banner = '/img/learnmore/topbar.svg'
subhead = 'Phase 2'
image = '/img/learnmore_phase02.svg'
layout = "phase"
linktext = "Read the Projects chapter in 'About Metadata 20/20'"
linkurl = "https://docs.google.com/document/d/1JQdNsrTLlYuKhyVUu_NuYZCPnj1IJNIRDYprWv7rSfM/edit#heading=h.ylxhemtap5q9"
navid = "projects"
+++

{{% learn-more/top-section %}}

The work of highlighting the insights from the distinct stakeholder groups (librarians, researchers, publishers, etc.) illuminated the systemic nature of addressing the metadata problems. Our work would not be accomplished by single stakeholder groups making incremental changes. Rather, the system itself of how we as an integrated community engage with, value and benefit from metadata would need to be improved to realize lasting improvement. 

Recognizing this needs, the Metadata 20/20 effort shifted to project-based work that included members of each of the stakeholder groups to address systemic challenges. The resulting six projects coalesced round a specific purpose statement to identify, research, and make recommendations to address the systemic challenge.

Each of these groups met over the course of a year to discuss and collaborate to further their purposes. The work of these projects led to the several output that are described in the next phase.

<!--{{% learn-more/link %}}-->
{{% /learn-more/top-section %}}


{{% learn-more/sections "phase_2" %}}