+++
title = "Join a Working Group"
shortTitle ="Join"
draft = false
date = "2020-11-19"
type = "domore"
banner = '/img/domore/topbar.svg'
linkid = 'join-a-working-group'
+++

{{% do-more/what-to-do %}}
* Consider which impact area you want to contribute to.
* Find an active working group participating in this area, using the resources below as a starting point.
* Get involved!
{{% /do-more/what-to-do %}}

{{< do-more/resources-wg >}}
