+++
title = "Take a class"
shortTitle = "Take"
draft = false
date = "2020-11-19"
type = "domore"
banner = '/img/domore/topbar.svg'
linkid = 'take-a-class'
+++

{{% do-more/what-to-do %}}
* List of steps
{{% /do-more/what-to-do %}}

{{% do-more/resources %}}
* Content here
{{% /do-more/resources %}}