+++
title = "Contribute a use case"
shortTitle ="Contribute"
draft = false
date = "2020-11-19"
type = "contribute-a-use-case"
banner = '/img/domore/topbar.svg'
linkid = 'contribute-a-use-case'
+++

{{< do-more/resources >}}

<ul>
  <li>Review the list of use cases below</li>
  <li>Get inspired by statements from researchers, librarians, publishers, providers and more about <a href="/learn-more/outcomes/">why they want richer, open metadata</a></li>
</ul>


{{< /do-more/resources >}}

