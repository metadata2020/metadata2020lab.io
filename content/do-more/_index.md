+++
title = "Do More"
draft = false
date = "2020-11-19"
type = "domore"
banner = '/img/domore/topbar.svg'
linkid = 'sign-the-pledge'
+++

<!--
{{< do-more/what-to-do >}}
<ul>
  <li>
    <div class='clearfix'>
      <div class="col-md-6 form">
          <a href="#open-signup">
              <div class='element'>
                  Sign the Pledge as an Individual
              </div>
          </a>
      </div>
    </div>
  </li>
  <li>
    <div class='clearfix'>
      <div class="col-md-6 form">
          <a href="/do-more/signatories">
              <div class='element'>
                  Sign the Pledge as an Organization
              </div>
          </a>
      </div>
    </div>
  </li>
</ul>

{{< /do-more/what-to-do >}}

{{< do-more/resources >}}
<div class='clearfix'>
  <div class="col-md-6 form">
      <a href="/do-more/signatories">
          <div class='element'>
              See the Signatories
          </div>
      </a>
  </div>
</div>
<ul>
  <li>See the <a href="/do-more">list of things</a> you can do to fulfill your pledge.</li>
  <li><a href="/learn-more">Learn more</a> about the Metadata 20/20 project and who was involved</li>
</ul>
{{< /do-more/resources >}} -->