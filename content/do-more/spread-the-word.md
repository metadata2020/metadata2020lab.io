+++
title = "Spread the Word about metadata"
shortTitle ="Spread"
draft = false
date = "2020-11-19"
type = "domore"
banner = '/img/domore/topbar.svg'
linkid = 'spread-the-word'
+++

{{% do-more/what-to-do %}}
* Identify opportunities to spread the word about metadata
* Adopt and use the resources below on your website and in your materials
* Encourage others to help spread the word
{{% /do-more/what-to-do %}}

{{% do-more/resources %}}

#### Talking Points: the why, who, and how about metadata

  * **Why is richer, open metadata important?** <br/>From a societal context, open metadata is a critical component of addressing some of the world’s greatest challenges. For example, rich, open metadata is an important component to realizing the critically important United Nations Sustainable Development Goals (SDGs). Without it, the goals will take longer, be more difficult to quantify, cost more and be harder to support.

  * **Who benefits from richer metadata?** <br />Richer metadata fuels discovery and innovation. Connected metadata bridges the gaps between systems and communities. Reusable, open metadata eliminates duplication of effort. When we settle for inadequate metadata, none of this is possible and everyone suffers as a consequence.

  * **How can I contribute to richer metadata? Learn more - Do more.** <br /> [Sign the Metadata Pledge](/do-more/sign-the-pledge), and choose from the set of [things you can do](/do-more).
<hr/>

#### Twitter Social Tiles

_Click the images to download them._

{{< figure src="/img/domore/dm-word_poverty.jpg" width=25% caption="Without open metadata ENDING POVERTY will take longer." target=blank link="/img/domore/dm-word_poverty.jpg">}}

{{< figure src="/img/domore/dm-word_hunger.jpg" width=25% caption="Without open metadata ZERO HUNGER will take longer." target=blank link="/img/domore/dm-word_hunger.jpg">}}

{{< figure src="/img/domore/dm-word_well-being.jpg" width=25% caption="Without open metadata GOOD HELATH AND WELL-BEING will cost more." target=blank link="/img/domore/dm-word_well-being.jpg">}}

{{< figure src="/img/domore/dm-word_sign.jpg" width=25% caption="Sign the Metadata Pledge!" target=blank link="/img/domore/dm-word_sign.jpg">}}

{{< figure src="/img/domore/dm-word_five-things.jpg" width=25% caption="Help make metadata richer, connected, reusable,and open for all research outputs to advance scholarly pursuits for the benefit of society." target=blank link="/img/domore/dm-word_five-things.jpg">}}

<hr/>

<!--

#### Slides for your presentations
  _Click on the links below to download a set of slides in your preferred format_
  * [Power Point](#)
  * [Google Slides](#)
  * [Apple Pages](#)

-->
{{% /do-more/resources %}}



