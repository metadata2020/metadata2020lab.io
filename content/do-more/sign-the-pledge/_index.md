+++
title = "Sign the Pledge"
shortTitle ="Sign"
draft = false
date = "2020-11-19"
type = "sign-the-pledge"
banner = '/img/domore/topbar.svg'
linkid = 'sign-the-pledge'
+++

{{< do-more/resources >}}

<ul>
  <li>See the <a href="/do-more">list of things</a> you can do to fulfill your pledge.</li>
  <li>Explore <a href="/resources/metadata-practices/">metadata practices</a> that metadata Creators, Curators, Custodians and Consumer use to take action</li>
  <li><a href="/learn-more">Learn more</a> about the Metadata 20/20 project and who was involved</li>
  <li>Consider a framework for <a href="https://doi.org/10.3897/rio.6.e54176"></a> measuring the impact of your metadata efforts</li>
  <li>Review existing <a href="/resources/metadata-best-practices/">metadata best practices</a></li>
</ul>
{{< /do-more/resources >}}