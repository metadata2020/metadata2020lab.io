+++
title = "Educate yourself about metadata"
shortTitle = "Educate"
draft = false
date = "2020-11-19"
type = "domore"
banner = '/img/domore/topbar.svg'
linkid = 'educate-yourself-about-metadata'
+++

{{% do-more/what-to-do %}}
* Review the [Metadata 20/20 Principles](/resources/metadata-principles/) that advocate for us to be good metadata citizens
* Consider the [Metadata 20/20 Personas](/resources/metadata-personas/) that recognizes that needs from and responsibilities to metadata change we each play specific roles from time to time
* Understand the [practices for achieving the metadata principles](/resources/metadata-practices/)
* Explore the [Metadata 20/20 journey](/learn-more/)
* Explore past writings like the ones below

{{% /do-more/what-to-do %}}

{{% do-more/resources %}}

This section highlights just a few papers and resources that may be of interest. This is in no way a complete list. We highly recommend the following two resources for deeper explorations about the topic of metadata:

#### Metadata Literature Review

A peer-reviewed academic literature review published in 2019 and supported by Metadata 20/20. The purpose of this literature review is to identify the challenges, opportunities, and gaps in knowledge with regard to the use of metadata in scholarly communications. This paper compiles and interprets literature in sections based on the professional groups, or stakeholders, within scholarly communications metadata: researchers, funders, publishers, librarians, service providers, and data curators. It then ends with a 'bird's eye view' of the metadata supply chain which presents the network of relationships and interdependencies between stakeholders. This paper seeks to lay the groundwork for new approaches to present problems in scholarly communications metadata. 

  * <em>Gregg WJ, Erdmann C, Paglione LAD, Schneider J, Dean C (2019) <strong>A literature review of scholarly communications metadata.</strong> Research Ideas and Outcomes 5: e38698. https://doi.org/10.3897/rio.5.e38698</em>

#### ScienceOpen Collection of Metadata Resources

This significant collection is community-curated and constantly evolving set of resources on the topic of the use of metadata in scholarly communications. Information on how to add to this collection can be found on the ScienceOpen page.

* <em>Dawson S, Erdmann C, Gregg W. <strong>Metadata Collection.</strong> ScienceOpen. https://doi.org/10.14293/S2199-1006.1.SOR-UNCAT.CLOD0SO.v1</em>

---

#### Other Resources

##### Why Metadata

* ##### Digital Science White Paper: A New 'Research Data Mechanics'
  Research metadata forms the information network that connects researchers, research institutions, funders, publishers, and research service providers together. How well information flows across this community has direct implications on how well research activity can be supported, and fundamentally how efficiently we can move research forward. This whitepaper recasts how we think about metadata - not as a series of static records, but as objects that move between systems and organizations.

  - <em>Science, Digital; Porter, Simon (2016): Digital Science White Paper: A New ‘Research Data Mechanics’. Digital Science. Report. https://doi.org/10.6084/m9.figshare.3514859.v1</em>

* ##### Neilsen Book US/UK Study: The Importance of Metadata for Discoverability and Sales
  Results of a Nielsen Book analysis that illustrates the strong link between the completeness of the appropriate metadata and the resultant sales.
  
  - <em>[Neilsen Book US Study: The Importance of Metadata for Discoverability and Sales](https://www.editeur.org/files/Events%20pdfs/Supply%20chain%202016/Nielsen%20Book%20US%20Study%20The%20Importance%20of%20Metadata%20for%20Discoverability%20and%20Sales.pdf). Walter D. The Nielsen Company US, LLC; 2016. ISBN: 978-1-910284-31-5</em><br/>
  - <em>[Neilsen Book UK Study: The Importance of Metadata for Discoverability and Sales](https://www.nielsenisbnstore.com/uploads/10451_Nielsen_Book_UK_Study_The_Importance_of_Metadata_for_Discoverability_and_Sales_Digital_D6.pdf). Walter D. Nielsen Book Services Ltd; 2016. ISBN: 978-1-910284-30-8</em>

##### Training Resources

* ##### Fairdata Training Resources on Data Management
  These general resources about research resource preservation provide an overview of how to be effective when preparing your materials for long-term preservation with an eye toward FAIR principles - Findable, Accessible, Interoperable, and Reusable. The training videos and checklists are available in Finnish and English, and are targeted toward those who perform research artifact activities only occasionally, for example, when submitting a paper or archiving a dataset.

  - <em><strong>Fairdata.fi Training Information.</strong> Fairdata Services, Ministry of Education and Culture Finland, CSC-IT Center for Science; Accessed 2021-May. https://www.fairdata.fi/en/training/training/</em>

##### Uses: Citation

* ##### Joint Declaration of Data Citation Principles
  Data citation, like the citation of other evidence and sources, is good research practice and is part of the scholarly ecosystem supporting data reuse. This declaration provides a set of guiding principles for citing data.

  - Data Citation Synthesis Group: Joint Declaration of Data Citation Principles. Martone M. (ed.) San Diego CA: FORCE11; 2014 https://doi.org/10.25490/a97f-egyk

<!--
#### Other stuff that needs classification & descriptions

* ##### Industry Leaders’ Perspectives on the Digital Transformation Journey in Publishing
  < description >

  - CITATION: https://drive.google.com/open?id=0B7M5zgYhPQbQT3FtZ2U0TWZxN2s

* ##### Doing the Time Warp: The Lag Between Publication and Discovery
  < description >

  - Lettie Y. Conrad. Doing the Time Warp: The Lag Between Publication and Discovery. Scholarly Kitchen, March 16th, 2017 https://scholarlykitchen.sspnet.org/2017/03/16/time-warp-lag-publication-discovery/

* ##### Zen and the Art of Metadata Maintenance
  < description >

  - John W. Warren. Zen and the Art of Metadata Maintenance. The Journal of Electronic Publishing, Volume 18, Issue 3. http://dx.doi.org/10.3998/3336451.0018.305

* ##### The Matter with Metadata: Completeness, Correctness and Consistency
  < description >

  - Natalie Guest, Ixxus. The Matter with Metadata: Completeness, Correctness and Consistency. The Literary Platform (2016) http://theliteraryplatform.com/magazine/2016/03/the-matter-with-metadata-why-are-taxonomies-still-so-taxing/

* ##### A renaissance in library metadata? The importance of community collaboration in a digital world. Insights.
  < description >

  - Bull, S. & Quimby, A., (2016). A renaissance in library metadata? The importance of community collaboration in a digital world. Insights. 29(2), pp.146–153. DOI: http://doi.org/10.1629/uksg.302

Juliane Schneider, Harvard Catalyst, [Notes](https://drive.google.com/open?id=0B7M5zgYhPQbQZUFzN0x5TVF0R3M) - needs to be moved to a more permanent home!

Kathryn.Stine@ucop.edu
[here are slides + notes](https://drive.google.com/file/d/0B3MZ4I0OlklXdUxISDRlcC1NekU/view?usp=sharing) <<< needs new home...  DLF last year on Zephir

DLF Metadata Assessment Working Group activities have some tie-ins -- https://wiki.diglib.org/Assessment:Metadata , http://dlfmetadataassessment.github.io/ , and use cases at https://docs.google.com/document/d/1xHT_lnIYaN7sxNgu0QzVqeyTU2UFXU8852khEBhQ_DM/edit

[The Continuum of Metadata Quality: Defining, Expressing, Exploiting](https://ecommons.cornell.edu/handle/1813/7895)
Bruce, Thomas R.; Hillmann, Diane I.
-->

{{% /do-more/resources %}}