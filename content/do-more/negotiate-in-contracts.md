+++
title = "Negotiate Metadata in Contracts"
shortTitle = "Negotiate"
draft = false
date = "2020-11-19"
type = "domore"
banner = '/img/domore/topbar.svg'
linkid = 'negotiate-in-contracts'
+++



{{% do-more/what-to-do %}}
* Hear from those who have done this work

  #### **WEBINAR: Negotiating Open Metadata into your Contracts**
  _RECORDED: 23 September 2021_
  
  <script src="https://fast.wistia.com/embed/medias/w89wokco23.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_w89wokco23 videoFoam=true" style="height:100%;position:relative;width:100%"><div class="wistia_swatch" style="height:100%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:100%;"><img src="https://fast.wistia.com/embed/medias/w89wokco23/swatch" style="filter:blur(5px);height:100%;object-fit:contain;width:100%;" alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;" /></div></div></div></div>

  Contract negotiations are important tools for incentivizing publishers and vendors to move to more open business models.  Researchers and research institutions have been working together to best plan for and accomplish this shared vision.  As part of this transformative process, open metadata must be included to ensure broad access to scholarly resources and other research outputs.

  As part of Metadata 20/20’s “Your Turn” campaign, the community has expressed interest in exploring and sharing how open metadata can be negotiated into contract language for those accessing and using scholarly resources and other research outputs. 

  **Speakers**

  ![Webinar Speakers](/img/domore/dm-metadata_in_contracts_speakers.png)

  * Adam Der, Max Planck Digital Library
  * John Chodacki, Metadata 20/20 Convenor
  * Laura Hanscom, Massachusetts Institute of Technology - MIT
  * Lisa Mackinder, California Digital Library - CDL
  * Catherine Nancarrow, FORCE11 Researcher Bill of Rights and Principles Working Group
  * Johan Rooryck, cOAlition S/ Leiden University
  * and other members of the Metadata 20/20 community

{{% /do-more/what-to-do %}}


{{% do-more/resources %}}
Expand your understanding of including open metadata in contracts. Use this list of resources to get you started. Know of a resource that is not included below? [Email us the information](mailto:lpaglione@metadata2020.org) and we'll consider its inclusion in our next update.

#### Background Reading

If you are just getting started, all of this information could be overwhelming. This section includes a few "level-setting" articles that provide the basics.

  * [Transformative Agreements: A Primer (By Lisa Janicke Hinchliffe; The Scholarly Kitchen - Apr 23, 2019)](https://scholarlykitchen.sspnet.org/2019/04/23/transformative-agreements/)
  * [Future of Scholarly Publishing and Scholarly Communication (European Commission - January 2019)](https://www.eosc-portal.eu/sites/default/files/KI0518070ENN.en_.pdf)

&nbsp;

#### Contract Guidelines

* **ESAC Guidelines for Transformative Agreements**

  This resource provides fundamental guidelines of transformative agreements, as defined by the ESAC Initiative (Efficiency and Standards for Article Charges) community. 

    * [ESAC Guidelines for Transformative Agreements](https://esac-initiative.org/about/transformative-agreements/guidelines-for-transformative-agreements/)
    * [ESAC Workflow Recommendations for Transformative Agreements](https://esac-initiative.org/about/oa-workflows/)
    * [International Collection of Negotiation principles](https://esac-initiative.org/guidelines/)
    * [Sample agreement terms](https://esac-initiative.org/workflows/sample-workflow-terms-for-transformative-agreements/)

&nbsp;

* **MIT Framework for Publishing Contracts**

  As MIT continues to lead in transforming scholarly communications from a system focused primarily on paywalled journal articles towards a system providing open access to the products of the full research life-cycle, many MIT scholars continue to value the services provided by journals and journal publishers. Those services include, but are not limited to, editorial oversight, curation, and coordination of the submission and peer review processes. Increasingly, scholars also value the availability of scholarly content as a corpus that is stored, described, and accessible for non-consumptive, computational access and analysis.

  The MIT Framework for Publisher Contracts guides MIT's negotiations with publishers, in support of their goals. While this framework guides their relationship regardless of the actions of any of their peer institutions or organizations, they recognize many others that have enforced this framework.

    * [MIT Framework for Publisher Contracts](https://libraries.mit.edu/scholarly/publishing/framework/) - _Written by the MIT Ad Hoc Task Force on Open Access to MIT’s Research, MIT Faculty Committee on the Library System, and MIT Libraries. © Massachusetts Institute of Technology. Licensed under a [Creative Commons Attribution-ShareAlike 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license._

&nbsp;

#### Guiding Statements

_Successful negotiations usually have clear values and objectives that guide the prioritization of negotiating points. These resources are examples of statements, principles, and values that can be used for prioritization._

* **Charters and principles in scholarly communication**

  This resource contains a list of declarations and other statements in the field of research, scholarly communication, data and publishing. It was initially compiled by Bianca Kramer and Jeroen Bosman. The list includes the year that each item was created, the name and link to the item, its aspect (code, data, open access, etc), the organization that supported the item, and information about the signatories. 

  _(This resource contains many more guiding statements than listed on this page.)_

    * [Charters and principles in scholarly communication (compiled by the FORCE11 Scholarly Commons Working Group)](https://docs.google.com/spreadsheets/d/1-aRXFiRg-VL9hpLpxoJqX6-OC-A0R2oCogHfIx52Nug/edit?usp=sharing)         

&nbsp;

* **Guiding Principles on Management of Research Information and Data**

  Established in February 2020, the [Dutch taskforce on Responsible Management of Research Information and Data](https://www.vsnu.nl/files/documenten/Domeinen/Onderzoek/Open%20access/Engelstalige%20samenvatting%20opdracht%20werkgroep.pdf) aims to establish terms and conditions under which metadata related to publicly funded research can be (re)used and enriched by public and private organisations.

    * [Overview](https://vsnu.nl/nl_NL/nieuwsbericht/nieuwsbericht/596-consultation-on-guiding-principles-on-management-of-research-information-and-data.html)
    * [Guiding Principles on Management of Research Information and Data (May 2020)](https://www.vsnu.nl/files/documenten/Domeinen/Onderzoek/Open%20access/Engelstalige%20samenvatting%20opdracht%20werkgroep.pdf)

&nbsp;

* **AmeliCA, Open Knowledge/Redalyc Principles and Values**

  AmeliCA is a communication infrastructure for scholarly publishing and open science. Sustained cooperatively, this initiative focuses on a non-profit publishing model to preserve the scholarly and open nature of scientific communication.

  AmeliCA commenced as Open Knowledge for Latin America and the Global South. On August 2019, however, faced with a regional context where platforms, science national councils, academic institutions and part of the scholarly community look down on local publishing by being compliant with commercial publishers’ strategies, and in the presence of an international context where initiatives such as Plan S define open science as a route, AmeliCA and Redalyc took concerted action to strengthen the non-profit publishing model to preserve the scholarly and open nature of scientific communication —also known as diamond model— beyond the Global South.

    * [AmeliCA Principles and values](http://amelica.org/index.php/en/principles-and-values/)

&nbsp;

* **Berlin Declaration on Open Access to Knowledge in the Sciences and Humanities**

  Drafted with the spirit of the Declaration of the Budapest Open Access initiative, the ECHO Charter, and the Bethesda Statement on Open Access Publishing, this Declaration Declaration provides a definition of an Open Access Contribution, and outlines the steps required to make progress toward the transition to the Electronic Open Access Paradigm. 

  The Declaration has been signed by nearly 300 research institutions, libraries, archives, museums, funding agencies, and governments from around the world. The geographic and disciplinary diversity of the support for the Berlin Declaration is illustrated by the signatories, which range from the leaders of the Max Plank Society to the Chinese Academy of Sciences, to Academia Europaea. Most recently, both Harvard University and the International Federation of Library Associations added their names to the roster of signatories.

    * [Berlin Declaration on Open Access to Knowledge in the Sciences and Humanities (2003)](http://www.berlin9.org/about/declaration/)


&nbsp;

* **Budapest Open Access Initiative**

  The Budapest Open Access Initiative arises from a small but lively meeting convened in Budapest by the Open Society Foundations (OSF) on December 1-2, 2001. The purpose of the meeting was to accelerate progress in the international effort to make research articles in all academic fields freely available on the internet. The participants represented many points of view, many academic disciplines, and many nations, and had experience with many of the ongoing initiatives that make up the open access movement. In Budapest they explored how the separate initiatives could work together to achieve broader, deeper, and faster success. They explored the most effective and affordable strategies for serving the interests of research, researchers, and the institutions and societies that support research. Finally, they explored how OSI and other foundations could use their resources most productively to aid the transition to open access and to make open-access publishing economically self-sustaining. The result is the Budapest Open Access Initiative. It is at once a statement of principle, a statement of strategy, and a statement of commitment.

  The initiative has been signed by the Budapest participants and a growing number of individuals and organizations from around the world who represent researchers, universities, laboratories, libraries, foundations, journals, publishers, learned societies, and kindred open-access initiatives. We invite the signatures, support, and participation of the entire world scientific and scholarly community.

    * [Original: Budapest Open Access Initiative (2002)](https://www.budapestopenaccessinitiative.org/read)
    * [Reaffirmation & Update: The Budapest Open Access Initiative after 10 years (2012)](https://www.budapestopenaccessinitiative.org/boai-10-recommendations)
    * [Open Access: Toward the Internet of the Mind - Reflection by Jean-Claude Guédon](https://www.budapestopenaccessinitiative.org/boai15/Untitleddocument.docx)

&nbsp;

#### Initiatives

_It is sometimes useful to leverage independent, regional, or global initiatives in contract negotiations. This section highlights several initiatives that may be relevant to open metadata._

* **Plan S**

  Plan S is an initiative for Open Access publishing that was launched in September 2018. The plan is supported by cOAlition S, an international consortium of research funding and performing organisations. Plan S requires that, from 2021, scientific publications that result from research funded by public grants must be published in compliant Open Access journals or platforms.

    * [The Plan S Principles and Implementation](https://www.coalition-s.org/addendum-to-the-coalition-s-guidance-on-the-implementation-of-plan-s/principles-and-implementation/)
    * [cOAlition S - a coordinated effort to implement Plan S](https://www.coalition-s.org/oa-champion-announced/)

&nbsp;

#### Involving Researchers in Negotiations

_Researchers can be both beneficiaries and contributors to the metadata that you may be negotiating in your contracts. These resources provide information about efforts about involving researchers._

* **Recorded talk: Putting our Money Where our Mouth Is - R. Schneider _(FORCE11 2018)_**

  Faculty at the University of California (UC) have endorsed a set of 18 principles that they propose be taken into account when UC engages in its upcoming and future journal license negotiations with commercial publishers. These broad-ranging principles were devised with input from various stakeholders across UC and at other like-minded academic institutions. They are intended to help restore the balance of power in the publishing system and give researchers control over the fruits of their own labor. Faculty expect that these principles will lead UC to push for terms and conditions in publisher agreements that are transformative and closely aligned with the goal of making scholarly communication more fair, transparent, sustainable, and open.

    * [Slides](https://zenodo.org/record/1458260#.YVR6hWZKgqs)
    * [Recorded presentation](https://www.youtube.com/watch?v=0Rrf6-btnuE&list=PLtYOue8U25N2jBWo-iKIlZkUmAqOYemqn&index=6)

&nbsp;

* **Declaration of Researcher Rights in Negotiating the Future of Scholarly Communications _(FORCE11)_**

  The FORCE11 Researcher Bill of Rights and Principles Working Group was formed to collect these existing statements and build an exemplar blueprint for future researcher-led declarations around the world to reference and for institutions and researchers to use in contractual negotiations and discussions. Resources:

    * [Introductory Blog Post](https://www.force11.org/blog/negotiating-future-scholarly-communication)
    * [The Declaration and a user guide for its use](https://zenodo.org/record/5013929#.YUZsM2ZKg6U)
    * [WEBINAR: Public Forum: Researcher Bill of Rights and Principles](https://www.youtube.com/watch?v=D-FMuDCIjwA)
    * [Working Group page](https://www.force11.org/group/researcher-bill-rights-and-principles)

&nbsp;

#### Other Resources

  * [GRC Action Plan Toward Open Access](https://www.globalresearchcouncil.org/fileadmin/documents/GRC_Publications/grc_action_plan_open_access_FINAL.pdf)
  * [JUSTICE Study of OA publishing and APC spend in Japan](https://oa2020.org/wp-content/uploads/pdfs/B13_Jun_Adachi.pdf)
  * [Listing of negotiation principles and roadmaps implemented by members of our global community](https://oa2020.org/take-action/#create)
  * [MIT Framework for Publisher Contracts](https://libraries.mit.edu/scholarly/publishing/framework/)
  * [LIBER Open Access:  Five Principles for Negotiations with Publisher](https://libereurope.eu/blog/2017/09/07/open-access-five-principles-for-negotiations-with-publishers/)
  * [Recommendations for the Transition to Open Access in Austria](https://zenodo.org/record/51799#.XweW2ZNKjlw)
  * [Science Europe Position Statement](https://nkfih.gov.hu/hivatalrol/open-access/science-europe-position)
  * [San Francisco Declaration on Research Assessment](https://cetaf.org/sites/default/files/documents/san_francisco_declaration._final_dora.pdf)
  * [Open Access: Principles, Practice and Potential](https://pubs.acs.org/doi/10.1021/acsomega.7b00707)
  * [Sweden: PROPOSAL FOR NATIONAL GUIDELINES FOR OPEN ACCESS TO SCIENTIFIC INFORMATION](https://www.vr.se/download/18.2412c5311624176023d25590/1555426972107/Proposal-nat-guidelines-open-access_VR_2015.pdf)
  * [The Culture of Scientific Research](https://libereurope.eu/blog/2017/09/07/open-access-five-principles-for-negotiations-with-publishers)
  * [The LERU Roadmap Towards Open Access](https://www.leru.org/publications/the-leru-roadmap-towards-open-access)
  * [The Scholarly Commons Principles](https://www.force11.org/scholarly-commons/principles)
  * [UCOLASC OA Principles](https://senate.universityofcalifornia.edu/_files/committees/ucolasc/scholcommprinciples-20180425.pdf)

  * [The Lens Collective Action Project](https://www.lens.org/lens/collective-action)
  * [MIT’s The Future of Libraries Report](https://future-of-libraries.mit.edu/)
  * [OA Switchboard](https://www.oaswitchboard.org/)

{{% /do-more/resources %}}
