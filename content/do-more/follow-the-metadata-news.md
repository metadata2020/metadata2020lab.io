+++
title = "Follow the metadata news"
shortTitle ="Follow"
draft = false
date = "2020-11-19"
type = "domore"
banner = '/img/domore/topbar.svg'
linkid = 'follow-the-metadata-news'
+++

{{% do-more/what-to-do %}}
* List of steps
{{% /do-more/what-to-do %}}

{{% do-more/resources %}}
* Content here
{{% /do-more/resources %}}