+++
title = "Events"
draft = false
date = "2018-01-07"
banner = '/img/backgrounds/Yellow_Banner.jpg'
style = "events"
+++

We are delighted to participate in the following events in 2018:

[APE 2018 - Academic Publishing in Europe](http://www.ape2018.eu/full-program/program); Berlin, Germany; 16-17 January
Presentation by Eva Mendez; [Slides](https://www.slideshare.net/ClareDean2/metadata-2020-at-ape-2018)

[PIDapalooza](https://pidapalooza18.sched.com/event/Cwmx/metadata-2020-harnessing-pid-power-for-the-greater-good); Spain; 23-24 January 2018
Presentation and discussion session hosted by Ginny Hendricks, John Chodacki, and Christopher Erdmann; [Slides](https://www.slideshare.net/ClareDean2/pidapalooza-2018-metadata-2020)

[Researcher to Reader](https://r2rconf.com/); London; 26 - 27 February, 2018
Workshop hosted by Ginny Hendricks and Ross Mounce

[2018 CSE Annual Meeting](https://www.councilscienceeditors.org/events/upcoming-events/2018-cse-annual-meeting/); New Orleans, LA; 5 - 9 May, 2018
Panel participation by Patricia Feeney

[Medical Library Association Annual Meeting](http://www.mlanet.org/meeting); Atlanta, GA; 18 - 23 May, 2018
Open meeting hosted by T. Scott Plutchak

[9th Annual VIVO Conference](http://vivoconference.org); Durham, NC; 6 - 8 June, 2018
Panel hosted by Chris Erdmann; [Slides](https://www.slideshare.net/ClareDean2/metadata-2020-vivo-conference-2018)

[NASIG 33rd Annual Conference](http://www.nasig.org/site_page.cfm?pk_association_webpage_menu=700); Atlanta, GA; 8 - 11 June, 2018
Presentation by Juliane Schneider

[PSP Books Committee Meeting](#); New York, NY; 15 June, 2018
Presentation by Jennifer Kemp

[Workshop on Open Citations](https://workshop-oc.github.io); Bologna, Italy; 3 - 5 September, 2018
Keynote by Ginny Hendricks

[CIG Conference 2018](https://www.cilip.org.uk/events/EventDetails.aspx?id=1086691&group=); Edinburgh, Scotland; 5 - 7 September, 2018
Presentation by Rachael Lamney

[FORCE2018](https://www.force11.org/meetings/force2018); Montreal, Canada; 10 October,
Pre-conference workshop led by Ted Habermann; [Register for the Workshop](https://goo.gl/forms/Kx6NoO09psd2UC0l2 )

[SciDataCon-IDW 2018](http://internationaldataweek.org); Gaborone, Botswana, 5 - 8 November, 2018
Presentation by Juliane Schneider and John Chodacki

---
Please email [info@metadata2020.org](mailto:info@metadata2020.org) if you'd like to see Metadata 20/20 on your event's agenda.
