+++
title = "Incentives for Improving Metadata Quality"
draft = false
date = "2018-03-01"
banner = '/img/backgrounds/Blue_Banner.jpg'
style = "projects"
+++

### Purpose

To highlight downstream applications and value of metadata for all parts of the community, telling real stories as evidence of how better metadata will meet their goals.

### Project plan

The following project plan is estimated to encompass work between May and October 2018.

* Discuss illustration of metadata flow through scholarly communications in consultation with community groups
* In consultation with community groups, start to develop fuller lists of incentives
* Create a detailed list of outputs for phase 2 of the project
* Gather material for re-use in business cases
* Align incentives list with P.5’s best practice guidelines
* Discuss outreach & education plans for time when first content is available for distribution, including proposals for upcoming conferences

### Challenges

* It is difficult to describe the importance of metadata conceptually, and importance needs to be grounded in real life
* Many organizations have one or two metadata ‘evangelists’ but they often lack the decision-making authority on budget or development roadmaps
* The responsibility for metadata quality is often outsourced or seen as an operational after-product, not a strategic priority

### Possible solutions to explore

* Gather value stories for each community group, understand their motivations and incentives
* Highlight and tell the stories of downstream metadata applications by gathering evidence of metadata leading to e.g. higher x (where x may = usage, discoverability, revenue, trustworthiness… etc.)
* Identify compelling channels through which to deliver these stories and work with communications project to disseminate incentive stories in a compelling way


### Group participants

{{% row %}}
{{% column %}}

* **Fiona Counsell**, Taylor & Francis (Group Lead)
* Barbara Chen, Modern Language Association
* Daniel Shanahan, F1000
* David Mellor, Center for Open Science
* David Schott, Copyright Clearance Center
* Despoina Evangelakou, SAGE Publishing
* Dom Mitchell, DOAJ
* Ed Moore, SAGE
* Emilie David, AAAS
* Fiona Bradley, Research Libraries UK
* Ginny Hendricks, Crossref
* Helen King, BMJ
* Jim Swainston, Emerald Group Publishing
* John Brucker,	Copyright Clearance Center
* Julie Zhu, IEEE
* Kathryn Kaiser, UAB School of Public Health

{{% /column %}}
{{% column %}}

* Krishna K., Molecular Connections Pvt. Ltd.
* Lola Estelle, SPIE
* Mahdi Moqri, RePEc
* Marianne Calilhanna, Cenveo Publisher Services
* Mark Donoghue, IEEE
* Michael Evans,	F1000
* Michelle Urberg, ProQuest
* Mike Taylor, Digital Science
* Neil Jefferies, Data2paper, SWORD
* Peter Kraker, Open Knowledge Maps
* Rachel Kessler, Proquest
* Ramamohan Paturi, UC San Diego
* Ravit David, OCUL, University of Toronto Library
* Sarah Whalen, AAAS
* Tony Alves, Aries Systems
* Tyler Ruse, Digital Science

{{% /column %}}
{{% /row %}}
