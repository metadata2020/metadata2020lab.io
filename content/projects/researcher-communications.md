+++
title = "Researcher Communications"
draft = false
date = "2018-03-01"
banner = '/img/backgrounds/Blue_Banner.jpg'
style = "projects"
+++

### Purpose

Exploring ways to align efforts between communities that aim to increase the impact and consistency of communication with researchers about metadata.

### Project plan

The following project plan is estimated to encompass work between May and October 2018.

* Review existing surveys and articles surrounding researcher attitudes to metadata
* Consider assigning a student to conduct a literature review
* Examine publishers who have improved metadata over last 2 years, and interview them about possible reasons (including how they may have encouraged authors to deposit more complete metadata)
* Conduct informal interviews to researchers in different fields to inform survey questions
* Create survey
* Find channels for survey distribution
* Collect results

### Challenges

* It is challenging to motivate authors and editors to provide accurate and complete metadata
* Authors are largely unaware of the downstream effects and benefits of high-quality metadata
* There is a tension between putting authors off by asking for too much metadata, or the same metadata they have been required to provide elsewhere, and securing sufficient metadata
* Researchers and readers use different terminology and have different needs and requirements depending on their field of work
* Researchers and readers will only respond to use cases that are highly relevant to them

### Possible solutions to explore

* Develop a suite of use cases, relevant to different fields
* Align with other projects (particularly ‘defining terms about metadata’ ‘metadata evaluation and guidance’ and ‘incentives for improving metadata’)
* Create a multi-channel outreach campaign for consistent communication via all communities in scholarly communications
* Work closely with funders to communicate new mandated metadata deposit with researchers

### OUTPUTS & RESOURCES

* **[Metadata Attitudes and Understandings](/learn-more/outcomes/survey-about-metadata)** - What do researchers and others think about metadata? What might help them find more value?
* **[Metadata Literature Review](../../blog/2019-08-05-metadata-literature-review/)** - How are academics thinking about metadata and characterizing its effectiveness? Where are there gaps in our insight that could benefit from further study?

### Group participants

{{% row %}}
{{% column %}}
<div style="font-size:1.2em;"><strong>Alice Meadows</strong>, ORCID (Co-Chair)</div>
{{% /column %}}
{{% column %}}
<div style="font-size:1.2em;"><strong>Michelle Urberg</strong>, ProQuest (Co-Chair)</div>
{{% /column %}}
{{% /row %}}
<iframe class="airtable-embed" src="https://airtable.com/embed/shrx7SwodsWCirQp6?backgroundColor=yellow" frameborder="0" onmousewheel="" width="100%" height="1000" style="background: transparent; border: 1px solid #ccc;"></iframe>
