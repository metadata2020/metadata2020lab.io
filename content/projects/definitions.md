+++
title = "Defining the Terms we use about Metadata"
draft = false
date = "2018-03-01"
banner = '/img/backgrounds/Blue_Banner.jpg'
style = "projects"
+++

### Purpose

In order to communicate effectively about anything, a common language must be acknowledged, tacitly or purposefully. In the metadata space, there is not agreement on what words like 'property', 'term', 'concept', 'schema', or 'title' refer to. This project will develop a glossary of words associated with metadata, both for core concepts and disciplinary areas.

### Project plan

The following project plan is estimated to encompass work between May and October 2018.

* Pilot researcher interviews surrounding metadata at Experimental Biology conference
* In consultation with other project and community groups, refine questions per community into a fuller survey for circulation surrounding metadata terminology
* In alignment with other community groups, distribute survey(s) and analyze results
* Contribute to central list of schema (P.2)
* Contribute to P.5's list of elements where needed
* Considering developing a “glossary” to clarify terminology

### Challenges

* We do not have consistent definitions to use for describing metadata across scholarly communications, which leads to misunderstandings and challenges in communicating issues and ideas for improvement
* Librarians, publishers, and researchers use different vocabulary depending on their fields, their needs, and their experiences
* Metadata vocabulary is particularly important in regards to accuracy (e.g. standardized date vocabulary important for accuracy)

### Possible solutions to explore

* List current definitions used and map to find most common uses
* Define core metadata glossary of terms e.g. “concept”, “schema”, “title”
* Evolve core metadata glossary to speak to different research fields, and different community group uses, while maintaining consistency

### Group participants

<div style="font-size:1.2em;"><strong>Scott Plutchak</strong>, University of Alabama at Birmingham (retired) (Chair)</div>

<iframe class="airtable-embed" src="https://airtable.com/embed/shrk2bcCMBMZ16R9A?backgroundColor=yellow" frameborder="0" onmousewheel="" width="100%" height="1000" style="background: transparent; border: 1px solid #ccc;"></iframe>
