+++
title = "Shared Best Practice and Principles"
draft = false
date = "2018-03-01"
banner = '/img/backgrounds/Blue_Banner.jpg'
style = "projects"
+++

### Purpose

To build a set of high level best practices for using metadata across the scholarly communication cycle, in order to facilitate interoperability and easier exchange of information and data across the stakeholders in the process, irrespective of chosen schema or standards.

### Project plan

The following project plan is estimated to encompass work between May and October 2018.

* Share and discuss list of tags/elements
* Survey Metadata 2020 population for best practices resources
* Catalog and publish list of best practices resources
* Create set of principles (e.g., FAIR)
* Follow up with hosting platforms and consultants that work with them to include what is included in RFPs, etc.
* Create white paper / statement to the community

### Challenges

* Lack of central core principles, best practices, and consistent guidance around scholarly communications means that everyone is doing their own thing, resulting in poor metadata, and lack of interoperability
* Researchers are defining their own standards due to lack of consistent direction/expectations, and these standards do not always result in  interoperable metadata
* For publishers, the biggest obstacle in synchronizing capture is the backlog of content published

### Possible solutions to explore

* Similar to FAIR principles, define core principles for metadata around scholarly communications, created and disseminated in easily digestible ways for different groups
* Discuss metadata ownership and governance

### OUTPUTS & RESOURCES

* **[Metadata Principles](../../resources/metadata-principles/)** - Compatible ⁍ Complete ⁍ Credible ⁍ Curated -- What does it mean to have “richer metadata”? How does context affect this equation?
* **[Metadata 2020 Practices](../../resources/metadata-practices)** - How do we live the Metadata Principles? What actions must we take?
* **[Metadata Best Practices](../../resources/metadata-best-practices/)** - What are the key metadata practices that are in use today?
* **[Metadata Use Cases](../../resources/metadata-use-cases/)** - How can use cases inform what to do next?


### Group participants

{{% row %}}
{{% column %}}
<div style="font-size:1.2em;"><strong>Howard Ratner</strong>, CHORUS (Co-Chair)</div>
{{% /column %}}
{{% column %}}
<div style="font-size:1.2em;"><strong>Jennifer Kemp</strong>, Crossref (Co-Chair)</div>
{{% /column %}}
{{% /row %}}
<iframe class="airtable-embed" src="https://airtable.com/embed/shrrxE1y3xoS3zYMR?backgroundColor=yellow" frameborder="0" onmousewheel="" width="100%" height="1000" style="background: transparent; border: 1px solid #ccc;"></iframe>
