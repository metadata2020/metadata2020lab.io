+++
title = "Projects"
draft = false
date = "2018-02-26"
banner = '/img/banners/banner-lego-projects-1.jpg'
style = "projects"
+++

The Metadata 20/20 [Community Groups](/communities) assessed their challenges and opportunities, and six themes emerged which will now be evolved as Projects.

Each Project welcomes voluntary participation from any individual from research communications who has expertise they think could benefit the work, and time to commit to meetings and offline work. If you are interested in contributing to a project, please fill out this [interest form](/projects/get-involved), and we’ll get back to you with more details.

Projects include:

### 1. Researcher communications

> Exploring ways to align efforts between communities that aim to increase the impact and consistency of communication with researchers about metadata.

##### [More about this project >](/projects/researcher-communications/)

### 2. Metadata recommendations and element mappings

> To converge communities and publishers towards a shared set of recommended metadata concepts with related mappings between those recommended concepts and elements in important dialects.

##### [More about this project >](/projects/mappings/)

### 3. Defining the terms we use about metadata

> In order to communicate effectively about anything, a common language must be acknowledged, tacitly or purposefully. In the metadata space, there is not agreement on what words like 'property', 'term', 'concept', 'schema', or 'title' refer to. This project will develop a glossary of words associated with metadata, both for core concepts and disciplinary areas.

##### [More about this project >](/projects/definitions)

### 4. Incentives for improving metadata quality

> To highlight downstream applications and value of metadata for all parts of the community, telling real stories as evidence of how better metadata will meet their goals.

##### [More about this project >](/projects/incentives)

### 5. Shared best practice and principles

> To build a set of high level best practices for using metadata across the scholarly communication cycle, in order to facilitate interoperability and easier exchange of information and data across the stakeholders in the process.

##### [More about this project >](/projects/shared-practice)

### 6. Metadata evaluation and guidance

> To identify and compare existing metadata evaluation tools and mechanisms for connecting the results of those evaluations to clear, cross-community guidance.

##### [More about this project >](/projects/evaluation-guidance)
