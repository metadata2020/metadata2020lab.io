+++
title = "Metadata Recommendations and Element Mappings"
draft = false
date = "2018-03-01"
banner = '/img/backgrounds/Blue_Banner.jpg'
style = "projects"
+++

### Purpose

To converge communities and publishers towards a shared set of recommended metadata concepts with related mappings between those recommended concepts and elements in important dialects.

### Project plan

The following project plan is estimated to encompass work between May and October 2018.

* Identify list of metadata schemas that are in use (“Metadata Schema Index”)
* Share list of schemas with MD2020 community to see if there are any missing and to get feedback on which are most used
* Collaborate with Project 5 to discuss list of tags/elements in each of the schemas of interest and identify concepts that cut across them
* Map concepts and element names in a table
* Produce diagram/poster to highlight similarities and differences

### Challenges

* Many communities and publishers develop recommendations for metadata in their disciplines or for data submitted with scientific papers. These recommendations typically include two elements: conceptual descriptions of metadata needs and representations of those concepts in community dialects (XML, JSON, RDF, …). Mapping between the recommended concepts is an important step towards converging towards a recommendation that is consistent across communities
* There are many different ways that metadata is created, vetted, used and distributed; and the complexity of this makes finding new efficiencies and systems implementation difficult
* Most groups face interoperability challenges with systems and processes
* There are silos within organizations themselves, making communications challenging

### Possible solutions to explore

* Identify concepts included in relevant community and publisher recommendations
* Identify concepts shared across recommendations
* Comprehensive metadata mapping across community groups can be used as a tool to communicate with others internally and externally to explain the uses, or misuses of metadata in context
* Mapping can reveal the complexity of metadata use to researchers (and in some cases publishers and librarians) to underscore the importance of completeness and accuracy
* Mapping will also help identify key areas of inefficiency, breakages, or gaps that the groups can then collaborate to address in further interoperability-related projects

### Group participants

{{% row %}}
{{% column %}}

* **Jim Swainston**, Emerald Group Publishing (Group lead)
* Adrian-Tudor Panescu, Figshare
* Angela Dappert, Springer Nature
* Bobbi Patham, Springer Nature
* Caroline Webber,	Aries Systems
* Christina Hoppermann, Springer Nature
* Daniel Berger, The American Water Works Association
* Eva Mendez, Universidad Carlos III de Madrid
* Helen King, BMJ
* Juliane Schneider, Harvard Catalyst
* Julie Zhu, IEEE
* Kathryn Kaiser, UAB School of Public Health
* Kathryn Sullivan, University of Manchester
* Krishna K., Molecular Connections Pvt. Ltd.

{{% /column %}}
{{% column %}}

* Mahdi Moqri, RePEc
* Maria Johnsson, Lund University
* Melissa Harrison, eLife
* Melissa Jones, Silverchair
* Natalie Pao, Apress (Springer Nature)
* Pam White	Kent, Hawaii Pacific University
* Patricia Feeney, Crossref
* Paul Dlug, American Physical Society
* Silvio Peroni, University of Bologna
* Tammy Moorse,	Hamilton Public Library
* Ted Habermann, HDF Group
* Vladimir Alexiev,	Ontotext

{{% /column %}}
{{% /row %}}
