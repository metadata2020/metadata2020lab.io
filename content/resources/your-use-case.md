+++
title = "Contribute your Metadata Use Case!"
draft = false
date = "2020-03-12"
banner = '/img/backgrounds/Blue_Banner.jpg'
style = "resources"
+++

##  Contribute a Metadata Use Case

Use cases are useful for identifying, clarifying and illustrating requirements and their possible solutions. Metadata 2020 has been gathering specific examples help illustrate the challenges and potential of working toward the Metadata [Best Principles](/metadata-principles/) and [Practices](../blog/2019-03-26-metadata-practices/).

Fill out the form below to contribute your use case. Submissions are moderated, and may be lightly edited for clarity before publication. 

Also see [Use Cases that have already been submitted](/resources/metadata-use-cases/).

<script src="https://static.airtable.com/js/embed/embed_snippet_v1.js"></script><iframe class="airtable-embed airtable-dynamic-height" src="https://airtable.com/embed/shr8kzcF6PUJidNL5?backgroundColor=yellow" frameborder="0" onmousewheel="" width="100%" height="3030" style="background: transparent; border: 1px solid #ccc;"></iframe>