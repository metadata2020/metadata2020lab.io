+++
title = "Metadata 2020 Practices"
draft = false
date = "2020-05-01"
banner = '/img/backgrounds/Blue_Banner.jpg'
style = "resources"
+++

## The Metadata 2020 Practices
Metadata 2020 is a collaboration that advocates richer, connected, and reusable, open metadata for all research outputs, which will advance scholarly pursuits for the benefit of society. During 2019, we released a set of [high-level, aspirational metadata principles](/resources/metadata-principles) designed to “advocate for all of us to be good metadata citizens.” These principles describe **what** we aspire to achieve.

These Metadata 2020 Practices describe **how** we achieve this state. Like the Principles, the Practices are context-sensitive and will be more goals than reality for some of us for some time to come, and that’s OK. What’s important and what we hope these combined resources support, is that the various stakeholders, described as [personas](/resources/metadata-personas), move toward a common set of goals, with support and guidance. 

To support this work, we have collected [metadata use cases](/resources/metadata-use-cases). These use cases are real-world examples designed to illustrate and enlighten and may be from practitioners and the curious alike. 

## Principles, Practices & Personas

### 1. Connect to what already exists

![Connect to what already exists graphic](/img/figures/principle1.svg)

* **Do**: Understand recommendations and context
* **Do**: Seek out and use existing metadata schema and practices
* **Do**: Use community resources and in-house expertise to contextualize requirements and maximize the opportunities they afford for discoverability

&nbsp; &nbsp; Performed by Metadata ![1](/img/figures/icon-creators.png) Creators; ![2](/img/figures/icon-curators.png) Curators; ![3](/img/figures/icon-custodians.png) Custodians; ![4](/img/figures/icon-consumers.png) Consumers <br/> &nbsp; &nbsp; Principles supported: <b style="background-color: #f57e30;">&nbsp;COMPATIBLE&nbsp;</b> &nbsp; &nbsp; <b style="background-color: #333f48; color: white;">&nbsp;CURATED&nbsp;</b>

### 2. Adopt schema best practices

![Adopt schema best practices graphic](/img/figures/principle2.svg)

* **Do**: Include [best practice](/resources/metadata-best-practices/) elements when possible
* **Do**: Include definitions and document schema rationale, e.g. label fields
* **Do**: Provide best practice elements for repositories and schemas

&nbsp; &nbsp; Performed by Metadata ![1](/img/figures/icon-creators.png) Creators; ![3](/img/figures/icon-custodians.png) Custodians <br/>&nbsp; &nbsp; Principles supported: <b style="background-color: #f57e30;">&nbsp;COMPATIBLE&nbsp;</b> &nbsp; &nbsp; <b style="background-color: #93b7be;">&nbsp;CREDIBLE&nbsp;</b>

### 3. Honor the strategic nature of metadata

![Honor the strategic nature of metadata graphic](/img/figures/principle3.svg)

* **Do**: Treat metadata as a strategic, primary output, like content
* **Do**: Consider broadly how rich metadata can benefit your organization by facilitating better discovery, and more linked data between resources

&nbsp; &nbsp; Performed by Metadata ![1](/img/figures/icon-creators.png) Creators; ![3](/img/figures/icon-custodians.png) Custodians <br/>&nbsp; &nbsp; Principles supported: <b style="background-color: #93b7be;">&nbsp;CREDIBLE&nbsp;</b> &nbsp; &nbsp; <b style="background-color: #333f48; color: white;">&nbsp;CURATED&nbsp;</b>

### 4. Prepare for evolving needs

![Prepare for evolving needs graphic](/img/figures/principle4.svg)

* **Do**: Sample and audit regularly to resolve gaps and errors
* **Do**: Make bulk updates when new elements are introduced
* **Do**: Consider ongoing investments to improve metadata
* **Do**: Provide for interoperability
* **Do**: Think beyond traditional formats and publishing paradigms, e.g. books, data and other formats that may not be digitized yet or need special attention
* **Don’t**: Assume research itself will not also evolve and change how it will be conducted in future

&nbsp; &nbsp; Performed by Metadata ![1](/img/figures/icon-creators.png) Creators; ![2](/img/figures/icon-curators.png) Curators; ![3](/img/figures/icon-custodians.png) Custodians <br/>&nbsp; &nbsp; Principles supported: <b style="background-color: #fdda00;">&nbsp;COMPLETE&nbsp;</b> &nbsp; &nbsp; <b style="background-color: #333f48; color: white;">&nbsp;CURATED&nbsp;</b>

### 5. Facilitate discoverability

![Facilitate discoverability graphic](/img/figures/principle5.svg)

&nbsp; &nbsp; Performed by Metadata ![1](/img/figures/icon-creators.png) Creators; ![2](/img/figures/icon-curators.png) Curators; <br/>&nbsp; &nbsp; Principles supported: <b style="background-color: #f57e30;">&nbsp;COMPATIBLE&nbsp;</b> &nbsp; &nbsp; <b style="background-color: #93b7be;">&nbsp;CREDIBLE&nbsp;</b>

* **Do**: Use verified PIDs (Persistent IDentifiers, e.g. ORCIDs) for all contributors, organizations, and content 
* **Do**: Use correct spelling and authoritative names from established vocabularies
* **Do**: Use complete publication dates
* **DO**: Continue to use interoperable data formats like UTF-8 and ISO 8601.
* **Do**: Include all contributors and their affiliations whenever possible
* **Do**: Favor electronic lookups and/or APIs to digitally obtain information directly from authoritative source systems
* **Don’t**: Re-key information or copy/ paste without proof-reading
* **Do**: Describe content in normalized, type-appropriate ways that are consistent with [best practices](/resources/metadata-best-practices/)
* **Don’t**: Use a unique-to-you approach. Augment community-specific approaches with PIDs and other standard metadata approaches

### 6. Adopt an attitude of continuous improvement

![Adopt an attitude of continuous improvement graphic](/img/figures/principle6.svg)

* **DO**: Respond to and address user requests for data correction
* **Do**: Follow the lead of users (including machines) and solicit feedback
* **Do**: Create feedback mechanisms for users to report their needs

&nbsp; &nbsp; Performed by Metadata ![1](/img/figures/icon-creators.png) Creators; ![2](/img/figures/icon-curators.png) Curators; ![3](/img/figures/icon-custodians.png) Custodians <br/>&nbsp; &nbsp; Principles supported: <b style="background-color: #333f48; color: white;">&nbsp;CURATED&nbsp;</b>

### 7. Act on issues when found

![Act on issues when you find them graphic](/img/figures/principle7.svg)

* **Do**: Utilize feedback mechanisms, e.g. to report errors 
* **Don’t**: Miss the opportunity to contribute to improvement

&nbsp; &nbsp; Performed by Metadata ![4](/img/figures/icon-consumers.png) Consumers <br/>&nbsp; &nbsp; Principles supported: <b style="background-color: #fdda00;">&nbsp;COMPLETE&nbsp;</b> &nbsp; &nbsp; <b style="background-color: #333f48; color: white;">&nbsp;CURATED&nbsp;</b>

* **Do**: Collaborate to solve problems
* **Don’t**: Work around problems

&nbsp; &nbsp; Performed by Metadata ![1](/img/figures/icon-creators.png) Creators; ![2](/img/figures/icon-curators.png) Curators; ![3](/img/figures/icon-custodians.png) Custodians; ![4](/img/figures/icon-consumers.png) Consumers <br/>&nbsp; &nbsp; Principles supported: <b style="background-color: #f57e30;">&nbsp;COMPATIBLE&nbsp;</b> &nbsp; &nbsp; <b style="background-color: #333f48; color: white;">&nbsp;CURATED&nbsp;</b>


### 8. Take a personal role in making metadata richer

![Take a personal role in making metadata richer graphic](/img/figures/principle8.svg)

* **Do**: Collaborate.Take an active role to advocate (demand, collaborate for) richer metadata 
* **Don’t**: Accept the status quo

&nbsp; &nbsp; Performed by Metadata ![4](/img/figures/icon-consumers.png) Consumers <br/>&nbsp; &nbsp; Principles supported: <b style="background-color: #333f48; color: white;">&nbsp;CURATED&nbsp;</b>
