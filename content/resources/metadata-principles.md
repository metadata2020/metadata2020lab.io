+++
title = "Metadata 2020 Principles"
draft = false
date = "2019-06-25"
banner = '/img/backgrounds/Blue_Banner.jpg'
style = "resources"
+++

## The Metadata 2020 Principles

Metadata 2020 is a collaboration that advocates richer, connected, and reusable, open metadata for all research outputs, which will advance scholarly pursuits for the benefit of society. We have created the following Principles based on existing best practices to complement and support FAIR data principles with an interpretation inclusive of both data and metadata.

<p style="font-size:1.4em;">These aspirational Principles were designed to encompass the needs of our entire community while ensuring thoughtful, purposeful, and reusable metadata resources. They advocate for all of us to be good metadata citizens. They provide a foundation for considering related work from Metadata 2020 and <strong>must be interpreted within the legal and practical context in which the communities operate.</strong></p>

<table>
  <thead>
    <tr><th colspan=2 style="line-height:1.8; font-size:1.5em;">METADATA PRINCIPLES</th></tr>
  </thead>
  <tbody>
    <tr><td colspan=2 style="font-size:1em;">&nbsp;<br/>For metadata to support the community, it should be</td></tr>
    <tr><td colspan=2 style="font-size:1em;"><b style="line-height:1.6; font-size:1.2em;">COMPATIBLE:</b> provide a guide to content for machines and people</td></tr>
    <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="font-size:1em;"><em>So, metadata must be as open, interoperable, parsable, machine actionable, human readable as possible.</em><br/>&nbsp;</td></tr>
    <tr><td colspan=2 style="font-size:1em;"><b style="line-height:1.6; font-size:1.2em;">COMPLETE:</b> reflect the content, components and relationships as published</td></tr>
    <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="font-size:1em;"><em>So, metadata must be as complete and comprehensive as possible.</em><br/>&nbsp;</td></tr>
    <tr><td colspan=2 style="font-size:1em;"><b style="line-height:1.6; font-size:1.2em;">CREDIBLE:</b> enable content discoverability and longevity</td></tr>
    <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="font-size:1em;"><em>So, metadata must be of clear provenance, trustworthy and accurate.</em><br/>&nbsp;</td></tr>  
    <tr><td colspan=2 style="font-size:1em;"><b style="line-height:1.6; font-size:1.2em;">CURATED:</b> reflect updates and new element</td></tr>
    <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="font-size:1em;"><em>So, metadata must be maintained over time.</em></td></tr>
  </tbody>
</table>
<p>&nbsp;</p>

<p>Also see our set of <a href="/resources/metadata-practices/">Recommended Actions and Practices</a> will illustrate use cases and provide some concrete action items to support the Practices.</p>
