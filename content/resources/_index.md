+++
title = "Resources"
draft = false
date = "2018-06-26"
banner = '/img/backgrounds/Blue_Banner.jpg'
style = "resources"
+++

Part of the Metadata 2020 mission is to provide the broader scholarly communications community with useful resources and guidance. Our project teams and sub-groups are in the process of building a number of new resources, reports, and publications; all of which will be available here once they are complete.  Please check back in regularly to see what's new!

Our work falls into two types of outputs, Guidance and Understanding.

## Guidance

Outputs that represent our thinking about metadata and how we feel that it should be characterized, managed, thought about, and valued.

### [Metadata Principles](/resources/metadata-principles/)

_from [Project 5 Shared Best Practice and Principles](../../projects/shared-practice/)<br/>STATUS: Published_

> Compatible ⁍ Complete ⁍ Credible ⁍ Curated<br/>What does it mean to have “richer metadata”? How does context affect this equation?

These aspirational Metadata 2020 Principles were designed to encompass the needs of our entire community while ensuring thoughtful, purposeful, and reusable metadata resources. They advocate for all of us to be good metadata citizens. They provide a foundation for considering related work from Metadata 2020 and must be interpreted within the legal and practical context in which the communities operate. These Principles are intended to guide the broadest possible cross-section of our community in improving research communications, publishing and discoverability. If they don’t speak your role in scholarly communications, we want to hear from you.

### [Metadata Personas](/resources/metadata-personas/)

_from [Project 4 Incentives for Improving Metadata Quality](../../projects/incentives/)<br/>STATUS: Published_

> Creators ⁍ Custodians ⁍ Curators ⁍ Consumers<br/>Who are the key players that will follow the principles to ensure the benefits?

From its start, Metadata 2020 has been engaged with the [various communities](../../communities/) that participate in metadata, Researchers; Publishers; Librarians; Data Publishers & Repositories; Services, Platforms & Tools; and Funders. In considering the flow of metadata between individuals and systems, we recognized that within these communities, people take on one or more personas based on what they are doing at any one moment. In addition, each persona’s actions are quite similar regardless of the community they belong to. Using this role-based set of personas is a useful construct for applying our project work. 

### [Metadata Practices](/resources/metadata-practices/)

_from [Project 5 Shared Best Practice and Principles](../../projects/shared-practice/)<br/>STATUS: Published_

> How do we live the Metadata Principles given the Metadata Personas that we assume? What activities must we do?

These Metadata 2020 Practices describe **how** we achieve the Metadata Principles. Like the Principles, the Practices are context-sensitive and will be more goals than reality for some of us for some time to come, and that’s OK. What’s important and what we hope these combined resources support, is that the various stakeholders, described as the Metadata Personas, move toward a common set of goals, with support and guidance. 

## Understanding

Outputs that provide insights into how we have classified and made sense of what currently exists and the comparison of these works to each other.

### [Metadata Best Practices](/resources/metadata-best-practices/)

_from [Project 5 Shared Best Practice and Principles](../../projects/shared-practice/)<br/>STATUS: Published_

> What are the key metadata practices that are in use today?

This output consists of links to existing metadata guidelines and best practices in order to shed light on what currently exists and whether they are applicable to the scholarly communications lifecycle. Best practices, in general, reflect agreed upon standards that help an industry or field to do its job better. The scholarly communications lifecycle works better when best practices are known and used by the community at all points of the cycle. The list is not meant to be comprehensive. If you think a resource is missing from this list, please let us know!

### [Metadata Use Cases](/resources/metadata-use-cases/)

_from [Project 5 Shared Best Practice and Principles](../../projects/shared-practice/)<br/>STATUS: Call for participation_

> How can use cases inform what to do next?

Use cases are useful for identifying, clarifying and illustrating requirements and their possible solutions. Metadata 2020 has been gathering specific examples help illustrate the challenges and potential of working toward the Metadata [Best Principles](/metadata-principles/) and [Practices](../blog/2019-03-26-metadata-practices/).

Do you have a use case to share? Please [contribute it](/resources/your-use-case/)!

### [Metadata Attitudes and Understandings](/learn-more/outcomes/survey-about-metadata)

_from [Project 1 Researcher Communications](../../projects/researcher-communications/)<br/>STATUS: Published_

> What do researchers and others think about metadata? What might help them find more value?

We launched a survey to gather feedback about metadata knowledge, attitudes, and usage in scholarly communications. Researchers were represented in the study, as were publishers, librarians, and repository managers. A survey to each of these groups produced results that confirmed known information: librarians, publishers, and repository managers on the whole know more about metadata than researchers. It also produced surprises: researchers and repository managers are more closely aligned in metadata needs than librarians and publishers. The gap between researchers and either librarians or publishers is worth studying further. Maybe lamentations about a lack of researcher investment by librarians and publishers is due in part to librarians and publishers?

### [Metadata Literature Review](../../blog/2019-08-05-metadata-literature-review/)

_from [Project 1 Researcher Communications](../../projects/researcher-communications/)<br/>STATUS: Published_

> How are academics thinking about metadata and characterizing its effectiveness? Where are there gaps in our insight that could benefit from further study?

<table>
  <tbody>
    <tr>
      <td style="width:20%;">&nbsp;</td>
      <td><b style="line-height:1.6; font-size:1.2;">
        Gregg WJ, Erdmann C, Paglione LAD, Schneider J, Dean C (2019) <em>A literature review of scholarly communications metadata.</em> Research Ideas and Outcomes 5: e38698. <a href=https://doi.org/10.3897/rio.5.e38698>https://doi.org/10.3897/rio.5.e38698</a>  
      </td>
      <td style="width:20%;">&nbsp;</td>
    </tr>
  </tbody>
</table>

<p>&nbsp;</p>

We share the publication of a peer-reviewed academic literature review which has been published in RIO Journal. The review presents insights gained from comparing a range of articles that address the challenges and opportunities present in scholarly communications metadata. 
