+++
title = "Metadata Use Cases"
draft = false
date = "2020-03-13"
banner = '/img/backgrounds/Blue_Banner.jpg'
style = "resources"
+++

## Metadata Use Cases

Use cases are useful for identifying, clarifying and illustrating requirements and their possible solutions. Metadata 2020 is gathering specific examples help illustrate the challenges and potential of working toward the Metadata [Best Principles](/resources/metadata-principles/) and [Practices](/resources/metadata-practices/).

As you explore these use cases, we recommend looking at the unfamiliar in addition to those that may be close to your work. There may be cases that are directly related to your needs, while other surface where your experiences might be applied.

Do you have a use case to share? Please [contribute it](/resources/your-use-case)!

<iframe class="airtable-embed" src="https://airtable.com/embed/shrgIN8IEkshzrayY?backgroundColor=yellow&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="1000" style="background: transparent; border: 1px solid #ccc;"></iframe>